[![Build Status](https://semaphoreci.com/api/v1/projects/9e3ed568-8bb9-46ae-976e-8baa5aa4fd77/562539/badge.svg)](https://semaphoreci.com/charleycesar/isiib)

#** Sistema Integrado de Igrejas Batias (iSIIB)** #

O mais novo sistema de gestão eclesiástica para igrejas Batistas de todo o país.
O iSIIB possui 2 repositórios:

1. Corresponde a todo o lado do servidor da aplicação (Models e Controllers) e foi desenvolvido em Ruby com o framework Rails. 
2. É o ClientISIIB que está no link abaixo que corresponde a toda parte do cliente da aplicação WEB. Ele foi inicialmente desenvolvido com html e js Utilizando AngularJS.


**Neste repositório iremos trabalhar APENAS com o iSIIB-SERVER**

##Informações Relevantes ##

* Iniciando o projeto
* Gemfile?
* Gems utilizadas
* Database
* RACK-CORS
* Testes e mais testes
* Documentação

### Iniciando o projeto ###
1. Levando em consideração que você já possua um conhecimento básico no RoR, faremos um tutorial de como iniciar o projeto em uma máquina zerada.
Primeiramente verifique se seu ruby está instalado e devidamente configurado:
```
#!bash

$ ruby --version
--Para instalar a Gem do pg
required sudo apt-get install libpq-dev
sudo gem install pg -v '0.18.1'


```
deve aparecer a versão 2.1 ou superior. 

2. Após instale a gem do rails em sua máquina:
```
#!bash

$ gem install rails
```

3. Rode o bundle para instalar as dependencias do projeto (Gemfile).
Obs: Certifique-se que o esteja na pasta raiz do projeto


```
#!bash

$ bundle install
```
4. Agora iremos instanciar nosso banco de dados. Vá para **config/database.yml** e 
edite as linhas 23,24 e 25 conforme o exemplo:




```
#!vim
username: [seu usuário do banco de dados]
password: [senha do banco]
host: [IP do banco]

```
**Obs: As configurações no reposítório já apontam para o DB de dev no escritório**

5.  Rodaremos o Migrate para que gere o banco de dados.


```
#!rails

 $ rake db:migrate
```

## Pronto, seu ambiente ambiente está pronto para uso! 
**Obs.: Mantenha o pensamento de TDD e de Documentação SEMPRE neste projeto**