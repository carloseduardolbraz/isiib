class SituacaosController < ApplicationController
  before_action :set_situacao, only: [:show, :edit, :update, :destroy]

  # GET /situacaos
  # GET /situacaos.json
  def index
    @situacaos = Situacao.where cliente_id: session[:ClientId]
  end

  # GET /situacaos/1
  # GET /situacaos/1.json
  def show
  end

  # GET /situacaos/new
  def new
    @situacao = Situacao.new
  end

  # GET /situacaos/1/edit
  def edit
  end

  # POST /situacaos
  # POST /situacaos.json
  def create
    msg = "Situação cadastrada com sucesso!"
    status = 201
    situacao_param = params[:situacao]    
    begin
      unless situacao_param[:descricao]
        raise "Descrição invalido!"
      end
      situacao = Situacao.new(situacao_param.to_hash)      
      unless situacao.save
        msg = ""    
        situacao.errors.each do |field, msg_erro|
          unless msg == ""
            msg << ' - ' + msg_erro 
          else
            msg << msg_erro
          end
        end
        raise msg
      end 
      render :json => {data: msg}.to_json, status: status            
    rescue => erro
      render :json =>  {data: erro.message}.to_json , status: :unprocessable_entity     
    end
  end

  # PATCH/PUT /situacaos/1
  # PATCH/PUT /situacaos/1.json
  def update
    respond_to do |format|
      if @situacao.update(situacao_params)
        format.json { render :show, status: :ok, location: @situacao }
      end
    end
  end

  # DELETE /situacaos/1
  # DELETE /situacaos/1.json
  def destroy
    @situacao.destroy
    respond_to do |format|
      format.html { redirect_to situacaos_url, notice: 'Situacao was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_situacao
      @situacao = Situacao.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def situacao_params
      params.require(:situacao).permit(:descricao)
    end
end
