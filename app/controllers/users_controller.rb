require 'jwt'
class UsersController < ApplicationController

  def new
    @user = User.new
  end

  # Description:: Usuários
	def index
		@users = User.all  		
	end

  # Description:: Teste   {Users\auntentication}  
  def auntentication
    begin
      login =  Base64.decode64(params[:login])              
      senha =  Base64.decode64(params[:password])              
      user  =  User.new            
      @usuario = user.logar({:login=>login, :password=>senha, :IgrejaId => session[:IgrejaId]})                    
      mensagem = @usuario
      status = 200
      unless @usuario
        mensagem = "Usuário ou Senha inválido"
        status = 401
      end        
        render :text => mensagem , :status=> status
    rescue => erro
      render :text => erro.message
    end
  end

 
  def create        
    list_permission = []   
    user = params["_json"]["user"]  
    group = params["_json"]["group"]  
    permission = params["_json"]["permission"]


    if ! user.present? 
      render :text => "Por favor informe os dados do usuário", status: :unprocessable_entity
    end

    if ! permission.present?
        render :text => "Por favor, seleciona as permissões do usuário", status: :unprocessable_entity
    end

    
    user = User.new login: Base64.decode64(user["login"]), password: Base64.decode64(user["password"]), email: user["email"]
    permission.each do |p|
        permissao = Permission.new ()
        permissao.level = p["level"]
        permissao.action_id = p["action_id"]
        user.permissions << permissao
    end

    if group.present?
        user.group_id = group[:id]
    end

    respond_to do |format|
      if user.save        
        format.json { render json: "O Usuario #{user.login} foi salvo com sucesso!".to_json, status: :created}
      else        
        format.json { render json: user.errors, status: :unprocessable_entity }
      end
    end
  end


  def getPermissions
    token = request.headers['Authorization']  
    
    authtoken = AuthToken.new(token)
    login = authtoken.payload['login']
    user = User.find_by_login(login)
    
    render :text =>user.permissionsView
  end



	#Métodos Privado
	private  	
	def user_params
		params.require(:user).permit(:login, :email, :password)
	end



end
