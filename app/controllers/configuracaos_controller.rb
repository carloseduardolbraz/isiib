class ConfiguracaosController < ApplicationController
	before_action :set_configuracao, only: [:update,:show]	  
	#PUT configuracaos/id.json
	def update
	    respond_to do |format|		
	    	configuracao_save = @configuracao.update(configuracao_params)

		    if configuracao_save
		      format.json { render :json =>{ data: "Configurações atualizadas com sucesso!"}.to_json , status: :ok, location: @configuracao }
		    else
		    	msg = ""
		    	configuracao_save.errors.each do |field, error|
		    		unless msg == ''
		    			msg << '\n'
		    		end
		    		msg << error
		    	end
		    end
	    end
  	end

  	def index
  		@configuracoes = Configuracao.all
  		@configuracoes = Configuracao.where(cliente_id: params[:cliente_id]) if params[:cliente_id]  		  		
  	end

  	def show
  		render :text => "asdf"
  	end

	def create				
		configuracao_saved = Configuracao.create configuracao_params
		if configuracao_saved.errors.count > 0
			msg = '';
			configuracao_saved.errors.each do |field,error|
				unless msg == ''
					msg << '\n'
				end
				msg << error
			end
			return render :text => {data: configuracao_saved.errors.to_json}.to_json			
		end
		render :text => {data: "Configuração cadastrada com sucesso!"}.to_json, status: :created
	end

	private 
	def set_configuracao
		begin
    		@configuracao = Configuracao.find params[:id]
    	rescue => error
    		render :json => {data: "Configuração não localizada!"}.to_json, status: :unprocessable_entity      
    	end

    end
	def configuracao_params
		params.require(:configuracao).permit(:id,:logo,:configurado,:cliente_id)
	end
end
