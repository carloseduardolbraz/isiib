class EstadosController < ApplicationController
  def index
  	@estados = Estado.order 'estado ASC'
  end

  def create
  	estado = Estado.create set_estado
  	render :json => estado.to_json
  end  

  private
  ##
  #  Devolve somente o parâmetro estado que chega na controller 
  #  @author Carlos Eduardo
  #  @return void
  #
  def set_estado
  	params.require(:estado).permit(:estado, :id)
  end
end
