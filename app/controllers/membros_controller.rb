class MembrosController < ApplicationController
  before_action :set_membro, only: [:show, :edit, :update, :destroy]
                               
  def index       
    @membros = Membro.where(nil)
    filter();
  end

  # Função para retornar membros com parametros de filtro
  # Recebe uma Requisição GET /membros.json?nome=Charley&page=1
  # @author Charley Oliveira <charleycesar@gmail.com>
  # @return [Object] {data=>[membros],paginate=>[opcoes de paginar] } Retorna os membro filtrados pelo parametro passado pela url[page,nome]
  def filter
    @membros = @membros.joins(:pessoa).includes(pessoa:[:telefones,:enderecos,:emails])
    @membros = @membros.where("nome ilike '%"+params[:nome]+"%'") if params[:nome].present?
    @membros = @membros.paginate(:page => params[:page], :per_page =>Isiib::PAGINATION[:limit]) if params[:page].present?
  end

  # GET /membros/1
  # GET /membros/1.json
  def show
  end
         
  # GET /membros/new
  def new
    @membro = Membro.new
  end

  # GET /membros/1/edit
  def edit
  end

  # POST /membros
  # POST /membros.json  
  def create
    msg = {data: "Membro Cadastrado com sucesso!"}.to_json
    status = 201
    membro = Membro.new
    #Salvando Observaçoes
    if params[:membro][:observacoes]
      unless params[:membro][:observacoes].kind_of? Array                
        raise 'Observação invalida'
      end
      params[:membro][:observacoes].each do |observacao|
        observacao_save = Observacao.new observacao.to_hash     
        membro.observacaos << observacao_save
      end    
    end
    
    #Convert to hash
    pessoa = Pessoa.new params[:pessoa].to_hash
    membro.pessoa = pessoa
    
    enderecos = params[:enderecos]
    telefones = params[:telefones]
    emails    = params[:emails]
    
    begin    
      #Obriga ter ao menos um endereco
      unless enderecos.kind_of? Array                
        raise 'Endereço não é um hash'
      end
      enderecos.each do |param_endereco|
        bairro = Bairro.find_by_bairro param_endereco[:bairro]
        #cria um novo bairro
        unless bairro
          bairro = Bairro.new
          bairro.bairro = param_endereco[:bairro]
          bairro.cidade_id = param_endereco[:cidade_id]
          bairro = bairro.save  
        end
        param_endereco.delete(:cidade_id)
        param_endereco.delete(:bairro)
        param_endereco.delete(:estado)
        endereco_save = Endereco.new param_endereco.to_hash
        endereco_save.bairro = bairro
        pessoa.enderecos << endereco_save
      end

      if telefones 

        unless telefones.kind_of? Array
          raise "Telefone não é um hash"
        end
        #Cadastra Telefones
        telefones.each do |telefone|
          #Salvar observação
          observacao = Observacao.new
          observacao.observacao = telefone[:observacao] 
          telefone.delete(:observacao);  
          telefone_save = Telefone.new telefone.to_hash
          telefone_save.observacaos << observacao
          pessoa.telefones << telefone_save
        end

      end

      if emails

        unless emails.kind_of? Array
          raise "Emails não é um hash"
        end
        #Cadastra Emails
        emails.each do |email|
          email_save = Email.new email.to_hash
          pessoa.emails << email_save
        end
      
      end

      unless pessoa.save   
        msg = ""    
        pessoa.errors.each do |field, msg_erro|
            msg << msg_erro + '\n'
        end
      end      
        render :text => msg, status: status      
    rescue => erro
      render :text => erro , status: :unprocessable_entity      
    end
  end

  # PATCH/PUT /membros/1
  # PATCH/PUT /membros/1.json
  def update
    respond_to do |format| 
      if @membro.update(membro_params)
        format.html { redirect_to @membro, notice: 'Membro was successfully updated.' }
        format.json { render :show, status: :ok, location: @membro }
      else
        format.html { render :edit }
        format.json { render json: @membro.errors, status: :unprocessable_entity }
      end
    end
  end
  # DELETE /membros/1
  # DELETE /membros/1.json
  def destroy
    @membro.destroy
    respond_to do |format|
      format.html { redirect_to membros_url, notice: 'Membro was successfully destroyed.' }
      format.json { head :no_content }
    end    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_membro      
      @membro = Membro.find(params[:id])      
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def membro_params

      params.require(:membro).permit(:data_batismo,:observacoes)
      params.require(:pessoa).permit(:cpf,:nome,:data_nascimento,:sexo,:forma_tratamento,:nacionalidade,:naturalidade,:grau_instrucao_id,:situacao_id)
      params.require(:enderecos).permit(:cep,:complemento,:logradouro,:numero,:bairro,:cidade_id,:tipo_endereco_id)
    
    end
end

