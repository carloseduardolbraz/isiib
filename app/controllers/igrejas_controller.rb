class IgrejasController < ApplicationController
  before_action :set_igreja, only: [:show, :update, :destroy]
  def new
    @igreja = Igreja.new
  end

  # Função para cadastrar uma igreja com seus dados de contato
  # Recebe uma Requisição POST 
  # @author Charley Oliveira <charleycesar@gmail.com>
  # @see {file:spec/factories/igrejas.rb} Formato dos dados
  # @param [JSON] data Contendo dados para cadastrar uma nova igreja
  # @option data [Object] :igreja Dados da Igreja
  # @option data [Object] :endereco Dados de Endereço
  # @option data [Object] :telefone Dados de Telefone
  # @option data [Object] :email Dados de E-mail
  # @return [String] "Igreja cadastrada com sucesso!" || Igreja.erro
  # @header [Numeric] :created || :unprocessable_entity
  def create     
    if params[:igreja]
      igreja = Igreja.new params[:igreja].to_hash
      if params[:endereco]
        if params[:endereco][:bairro]
          params_bairro = params[:endereco]
          bairro = Bairro.find_by_bairro params[:endereco][:bairro]
          #cria um novo bairro
          unless bairro
            bairro = Bairro.new
            bairro.bairro = params[:endereco][:bairro]
            bairro.cidade_id = params[:endereco][:cidade_id]
            bairro = bairro.save  
          end
          params[:endereco].delete(:cidade_id)
          params[:endereco].delete(:bairro)
        end
        endereco_save = Endereco.new params[:endereco].to_hash
        endereco_save.bairro = bairro
        igreja.enderecos << endereco_save
      end
      if params[:telefone]
        observacao = Observacao.new
        observacao.observacao = params[:telefone][:observacao] 
        params[:telefone].delete(:observacao)  
        telefone_save = Telefone.new params[:telefone].to_hash
        telefone_save.observacaos << observacao
        igreja.telefones << telefone_save
      end
      if params[:email]
        email_save = Email.new params[:email].to_hash
        igreja.emails << email_save
      end

      if igreja.save
        render :text=> "Igreja cadastrada com sucesso!".to_json, status: :created
      else
        msg = ""    
        igreja.errors.each do |field, msg_erro|
          msg << msg_erro + '\n'
        end
        render :json =>msg.to_json,  status: :unprocessable_entity
      end
    end
  end
  
  # Função para cadastrar uma congregação e seus dados de contato junto a uma determinada igreja
  # Recebe uma Requisição POST 
  # @author Charley Oliveira <charleycesar@gmail.com>
  # @param [id_igreja] Código de igreja
  # @see {file:spec/factories/igrejas.rb} Formato dos dados
  # @param [JSON] data Contendo dados para atualizar uma igreja
  # @option data [Object] :igreja Dados da Congregação
  # @option data [Object] :endereco Dados de Endereço
  # @option data [Object] :telefone Dados de Telefone
  # @option data [Object] :email Dados de E-mail
  # @return [String] "Congregação cadastrada com sucesso!" || Igreja.erro
  # @header [Numeric] :created || :unprocessable_entity
  def create_congregacao
    if Igreja.exists? params[:id_igreja]
      igreja = Igreja.find(params[:id_igreja])
    end

    if params[:igreja]
      congregacao = Igreja.new params[:igreja].to_hash
      igreja.igrejas << congregacao
      if params[:endereco]
        if params[:endereco][:bairro]
          params_bairro = params[:endereco]
          bairro = Bairro.find_by_bairro params[:endereco][:bairro]
          #cria um novo bairro
          unless bairro
            bairro = Bairro.new
            bairro.bairro = params[:endereco][:bairro]
            bairro.cidade_id = params[:endereco][:cidade_id]
            bairro = bairro.save  
          end
          params[:endereco].delete(:cidade_id)
          params[:endereco].delete(:bairro)
        end
        endereco_save = Endereco.new params[:endereco].to_hash
        endereco_save.bairro = bairro
        congregacao.enderecos << endereco_save
      end
      if params[:telefone]
        observacao = Observacao.new
        observacao.observacao = params[:telefone][:observacao] 
        params[:telefone].delete(:observacao)  
        telefone_save = Telefone.new params[:telefone].to_hash
        telefone_save.observacaos << observacao
        congregacao.telefones << telefone_save
      end
      if params[:email]
        email_save = Email.new params[:email].to_hash
        congregacao.emails << email_save
      end
      if igreja.save
        render :json=> { data: "Congregação cadastrada com sucesso!" }.to_json, status: :created
      else
        msg = ""    
        igreja.errors.each do |field, msg_erro|          
          msg << msg_erro + '\n'
        end
        render :text=> msg.to_json, status: :unprocessable_entity
      end
    end
  end

  # Função para atualizar os dados de uma igreja
  # Recebe uma Requisição PUT 
  # @author Charley Oliveira <charleycesar@gmail.com>
  # @param [id] Código de igreja
  # @see {file:spec/factories/igrejas.rb} Formato dos dados
  # @param [JSON] data Contendo dados para atualizar uma igreja
  # @option data [Object] :igreja Dados da Igreja
  # @option data [Object] :endereco Dados de Endereço
  # @option data [Object] :telefone Dados de Telefone
  # @option data [Object] :email Dados de E-mail
  # @return [String] "Dados da Igreja foram atualizados!" || Igreja.erro
  # @header [Numeric] :created || :unprocessable_entity
  def update
      if params[:igreja]
        @igreja.update(params[:igreja].to_hash)
        update_data_contato
        if @igreja.save
          render :text=> "Dados da Igreja foram atualizados!".to_json, status: :created
        else
          msg = ""    
          @igreja.errors.each do |field, msg_erro|
            msg << msg_erro + '\n'
          end
          render :text=> msg.to_json, status: :unprocessable_entity
        end
      end
  end

  # Função para atualizar os dados de uma congregação junto a uma determinada igreja
  # Recebe uma Requisição POST 
  # @author Charley Oliveira <charleycesar@gmail.com>
  # @param [id_igreja] Código de igreja
  # @param [id_congregacao] Código de congregação
  # @see {file:spec/factories/igrejas.rb} Formato dos dados
  # @param [JSON] data Contendo dados para atualizar uma congregação
  # @option data [Object] :congregacao Dados da Congregação
  # @option data [Object] :endereco Dados de Endereço
  # @option data [Object] :telefone Dados de Telefone
  # @option data [Object] :email Dados de E-mail
  # @return [String] "Congregação atualizada com sucesso!" || Igreja.erro
  # @header [Numeric] :created || :unprocessable_entity
  def update_congregacao
    @igreja = Igreja.find(params[:id_congregacao])
    if params[:congregacao]
        @igreja.update(params[:congregacao].to_hash)
        update_data_contato
        if @igreja.save
          render :text=> "Congregação atualizada com sucesso!".to_json, status: :created
        else
          msg = ""    
          @igreja.errors.each do |field, msg_erro|
            msg << msg_erro + '\n'
          end
          render :text=> msg, status: :unprocessable_entity
        end
      end
  end
  
  def destroy    
    @igreja.destroy
    respond_to do |format|
      format.html { redirect_to membros_url, notice: 'Membro was successfully destroyed.' }
      format.json { head :no_content }
    end
   
  end 

  def index            
    @igrejas = Igreja.where cliente_id: session[:ClienteId]
  end

  def show        
  end

  # Função para atualizar os dados de contato de uma igreja
  # @author Charley Oliveira <charleycesar@gmail.com>
  # @see {file:spec/factories/igrejas.rb} Formato dos dados
  # @param [JSON] data Contendo dados de contato
  # @option data [Object] :endereco Dados de Endereço
  # @option data [Object] :telefone Dados de Telefone
  # @option data [Object] :email Dados de E-mail
  # Nao há retorno é apenas uma parte de código para evidar repetição de códigos
  def update_data_contato
    if params[:endereco]
      if params[:endereco][:bairro]
        bairro = Bairro.find_by_bairro params[:endereco][:bairro]
        if bairro == nil
          bairro = Bairro.new
          bairro.bairro = params[:endereco][:bairro]
          bairro.cidade_id = params[:endereco][:cidade_id]
          if bairro.save 
            bairro = Bairro.last
          else
            raise "Houve um erro ao salvar o Bairro"
          end
        end
        params[:endereco].delete(:cidade_id)
        params[:endereco].delete(:bairro)
      end
      if @igreja.enderecos.count == 0
        endereco_save = Endereco.new params[:endereco].to_hash
        endereco_save.bairro = bairro
        @igreja.enderecos << endereco_save
      else
        endereco = @igreja.enderecos.first
        endereco.bairro = bairro
        endereco.update(params[:endereco].to_hash)
      end
    end
    if params[:telefone]
      if @igreja.telefones.count == 0
        observacao = Observacao.new
        observacao.observacao = params[:telefone][:observacao] 
        params[:telefone].delete(:observacao)  
        telefone_save = Telefone.new params[:telefone].to_hash
        telefone_save.observacaos << observacao
        @igreja.telefones << telefone_save
      else
        telefone = @igreja.telefones[0]
        obs = telefone.observacaos[0]
        if obs == nil
          observacao = Observacao.new
          observacao.observacao = params[:telefone][:observacao] 
          params[:telefone].delete(:observacao)  
          telefone.observacaos << observacao
        else
          obs.observacao = params[:telefone][:observacao]
          telefone.observacaos << obs
        end  
        params[:telefone].delete(:observacao)  
        telefone.update(params[:telefone].to_hash)
      end
    end
    if params[:email]
      if @igreja.emails.count == 0
        email_save = Email.new params[:email].to_hash
        @igreja.emails << email_save
      else
        @igreja.emails.first.update(params[:email].to_hash)
      end
    end
  end

  private
  # Função para Setar uma igreja
  # É antes das funções [:show, :update, :destroy]
  # @author Charley Oliveira <charleycesar@gmail.com>
  # @param [id] Código de igreja
  # @param [congregacao] Recebe uma Query String ?congregacao=true|false
  # Quando true o json.builder da igrejas.show retorna os dados da igreja e as congregações
  # Quando false o json.builder da igrejas.show retorna apenas os dados da igreja sem congregações
  # @return [Object] Retorna o objeto da igreja
  def set_igreja
    params.require(:id)
    @igreja = Igreja.new
    @igreja = Igreja.find(params[:id]) if Igreja.exists? params[:id]   
    @congregacao = params[:congregacao]  if params[:congregacao]     
  end


end
