class MinisteriosController < ApplicationController
  before_action :set_ministerio, only: [:show, :update, :destroy]

  # GET /ministerios
  # GET /ministerios.json
  # Função para retornar todos os ministerios
  # @author Charley Oliveira <charleycesar@gmail.com>
  # @return [Array] Retorna todos os ministerios
  # @header [Numeric] :ok
  def index    
    igreja_id = params[:igreja_id]
    @ministerios = Ministerio.where igreja_id: igreja_id
  end

  # GET /ministerios/:id
  # GET /ministerios/:id.json
  # Função para buscar um ministerio especifico
  # @author Charley Oliveira <charleycesar@gmail.com>
  # @param :id [Numeric]  Contendo o id para realizar a consulta
  # @return [Object] Retorna o ministerio requerido pelo id
  # @header [Numeric] :ok
  def show
  end

  # POST /ministerios
  # POST /ministerios.json
  # Função para cadastrar um ministerio
  # @author Charley Oliveira <charleycesar@gmail.com>
  # @see {file:spec/factories/ministerios.rb} Formato dos dados
  # @param [JSON] ministerio Contendo dados para cadastrar um novo ministerio
  # @option ministerio [Object] :ministerio Dados do ministerio
  # @return [String] "Ministerio cadastrado com sucesso!" || ministerio.erro
  # @header [Numeric] :created || :unprocessable_entity
  def create
  	@ministerio = Ministerio.new(ministerio_params)
    @ministerio.igreja_id = session[:IgrejaId]

    respond_to do |format|
      if @ministerio.save
        format.html { redirect_to @ministerio, notice: 'Ministerio was successfully created.' }
        format.json { render :show, status: :created, json: {data: "Ministério cadastrado com sucesso!"} }
      else
        format.html { render :new }
        format.json { 
          msg = ''
          @ministerio.errors.each do |campo,mensagem|
            msg << "\n #{mensagem}" unless msg == ''
            msg << "#{mensagem}" if msg == ''            
          end
          render json: msg.to_json, status: :unprocessable_entity 
        }
      end
    end
  end

  # PATCH/PUT /ministerios/:id
  # PATCH/PUT /ministerios/:id.json
  # Função para atualizar um ministerio especifico
  # @author Charley Oliveira <charleycesar@gmail.com>
  # @see {file:spec/factories/ministerios.rb} Formato dos dados
  # @param :id [Numeric] Contendo o Id do ministerio que será atualizado
  # @param [JSON] ministerio Contendo dados para atualizar um ministerio
  # @option ministerio [Object] :ministerio Dados do ministerio
  # @return [String] "Ministerio atualizado com sucesso!" || ministerio.erro
  # @header [Numeric] :ok || :unprocessable_entity
  def update
    respond_to do |format|
      if @ministerio.update(ministerio_params)
        format.html { redirect_to @ministerio, notice: 'Ministerio was successfully updated.' }
        format.json { render :show, status: :ok, text: "Ministerio atualziado com sucesso!".to_json }
      else
        format.html { render :edit }
        format.json { render json: @ministerio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ministerios/:id
  # DELETE /ministerios/:id.json
  # Função para deletar um ministerio especifico
  # @author Charley Oliveira <charleycesar@gmail.com>
  # @param :id [Numeric] Contendo o Id do ministerio que será deletado
  # @return [String] "Ministerio deletado com sucesso!"
  # @header [Numeric] :ok
  def destroy
    @ministerio.destroy
    respond_to do |format|
      format.html { redirect_to ministerios_url, notice: 'Ministerio deletado com sucesso.'.to_json }
      format.json { render :json, status: :ok, json: { data: "Ministerio deletado com sucesso." } }
    end
  end

  private
  	# Função utilizada para compartilhar configurações em comum entre outros metodos
    # Utilizado para setar uma instância ou retornar um erro caso não encontre o Ministerio requerido
    def set_ministerio
   		if !Ministerio.exists?params[:id]
   			render text: "Não foi encontrado ministério!".to_json
   		else
   			@ministerio = Ministerio.find(params[:id])
   		end
    end

    # Configuração dos Parametros 
    # White List Params 
    def ministerio_params      
      params.require(:ministerio).permit(:ministerio)
    end
end
