class GroupsController < ApplicationController
  # Description:: Grupos
  def index
  	
  	@groups = Group.all
  end


  
  


  # POST groups/index  
  def create  	    
  	group = groups_params
  	@group = Group.new(groups_params)
  	permissions = params[:permission]  
  	
  	permissions.each do |permission|  		
      p = Permission.new( level: permission[:level], action_id: permission[:action_id])      
    	@group.permissions << p	
   	end


    respond_to do |format|      
      if @group.save        
        format.html { redirect_to @group, notice: 'O Grupo #{@group.name.capitalize} foi cadastrado com sucesso!' }
        format.json { render :text=> "O Grupo #{@group.name.capitalize} foi cadastrado com sucesso!".to_json, status: :created }
      else
        format.html { render :new }
        format.json { render json: @group.errors.to_json, status: :unprocessable_entity }
      end
    end
  end

  private
  def groups_params
  	params.require(:group).permit(:name)
  end
end
