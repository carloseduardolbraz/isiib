class ApplicationController < ActionController::Base      
    before_filter :has_token, :has_authorization ,:if => :isnot_login?                 
    #  Verifica se o Request possui o token de autenticação
    #  Author:: Carlos Eduardo L. Braz   <carloseduardo@lanwise.com.br>
    #    
    def has_token      
      @token = request.headers['Authorization']                            
      if !@token.present?
          render :text=> 'Usuário não Autenticado' , status: :forbidden
      end      
    end

    #  Autentica a Request verificando se a sua Sessão ainda é válida e 
    #  se as o Token tem permissão para acessar alguma página
    #  Author:: Carlos Eduardo L. Braz   <carloseduardo@lanwise.com.br>
    #
    def authentication_token      
      authtoken  = AuthToken.new(@token)

      time_token = authtoken.payload["time_token"]
      #converte a String vinda do payload para um objeto ActiveSupport::WithTimeZone
      time_token = ActiveSupport::TimeZone['UTC'].parse(time_token)      
      if time_token < Time.now
        #Login Timout HTTP code status
        render :text=> "Tempo da Sessão(token) foi expirado", status: 419
      end
    end

    #  Verifica se o usuário logado possui acesso a uma action
    #
    #  Author:: Carlos Eduardo L. Braz    <carloseduardo@lanwise.com.br>    
    #    
    def has_authorization      
      permissao_request =  "#{params[:controller]}/#{params[:action]}"
      authtoken  = AuthToken.new(@token)
      login = authtoken.payload["login"]

      usuario = User.find_by_login(login)




      unless usuario.present?
        render :text => "Usuário não cadastrado no sistema", :status => 401
        return false
      end
      retorno = usuario.authorized? permissao_request
          
      unless retorno ||  params[:action] != "getPermissions"
        render :text => "Seu usuário não tem permissionsão para acessar esta parte do sistema". to_json, status: 401
      end
     
     
    end
    # @Description ::
    private
    #  Verifica se a requisição está tentando acessar  áreas do sistema que
    #  não são públicas
    #  Author:: Carlos Eduardo L. Braz    <carloseduardo@lanwise.com.br>
    #  Return:: {Bool}  
    def isnot_login?         
      #seta o id da igreja
      session[:IgrejaId] = request.headers['IgrejaId'].to_s                       
      session[:ClienteId] = request.headers['ClientId'].to_s                  

      action  = params[:action]      
      action != "auntentication"  && action != "getPermissions"
      return false
    end
end