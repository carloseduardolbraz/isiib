class OcupacaosController < ApplicationController
  def index
  	ministerio = params[:ministerio_id]
  	render :json => {data: "Selecione um ministerio"} unless ministerio
  	@ocupacaos = Ocupacao.where ministerio_id: ministerio  	
  	
  end
end
