class GrauInstrucaosController < ApplicationController
  before_action :set_grau_instrucao, only: [:show, :edit, :update, :destroy]

  # GET /grau_instrucaos
  # GET /grau_instrucaos.json
  def index
    @grau_instrucaos = GrauInstrucao.all
  end

  # GET /grau_instrucaos/1
  # GET /grau_instrucaos/1.json
  def show
  end

  # GET /grau_instrucaos/new
  def new
    @grau_instrucao = GrauInstrucao.new
  end

  # GET /grau_instrucaos/1/edit
  def edit
  end

  # POST /grau_instrucaos
  # POST /grau_instrucaos.json
  def create
    @grau_instrucao = GrauInstrucao.new(grau_instrucao_params)

    respond_to do |format|
      if @grau_instrucao.save
        format.html { redirect_to @grau_instrucao, notice: 'Grau instrucao was successfully created.' }
        format.json { render :show, status: :created, location: @grau_instrucao }
      else
        format.html { render :new }
        format.json { render json: @grau_instrucao.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /grau_instrucaos/1
  # PATCH/PUT /grau_instrucaos/1.json
  def update
    respond_to do |format|
      if @grau_instrucao.update(grau_instrucao_params)
        format.html { redirect_to @grau_instrucao, notice: 'Grau instrucao was successfully updated.' }
        format.json { render :show, status: :ok, location: @grau_instrucao }
      else
        format.html { render :edit }
        format.json { render json: @grau_instrucao.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /grau_instrucaos/1
  # DELETE /grau_instrucaos/1.json
  def destroy
    @grau_instrucao.destroy
    respond_to do |format|
      format.html { redirect_to grau_instrucaos_url, notice: 'Grau instrucao was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_grau_instrucao
      @grau_instrucao = GrauInstrucao.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def grau_instrucao_params
      params.require(:grau_instrucao).permit(:descricao)
    end
end
