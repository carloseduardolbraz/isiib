class TipoAssembleiaController < ApplicationController
	def index
		igreja_id = params[:igreja_id]
		unless igreja_id
			render :json => {data: "Selecione uma igreja"}
		end
		@tipos = TipoAssembleia.where(igreja_id: igreja_id ) 
	end
end
