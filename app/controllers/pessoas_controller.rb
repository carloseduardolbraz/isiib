class PessoasController < ApplicationController
    def index
        nome = params[:nome]    
        @pessoas = Pessoa.where("nome ilike '%"+nome+"%'") if nome
  	
        if params[:conditions]
    	   conditions = JSON.parse params[:conditions]  if params[:conditions]
    	   @pessoas = Pessoa.includes(:enderecos).where(conditions)   	
        end
        unless @pessoas.exists?
  		    return 	render :json => @pessoas, :status => 403
  	     end  	
    end
end
