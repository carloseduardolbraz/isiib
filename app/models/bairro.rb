class Bairro < ActiveRecord::Base
	has_many :enderecos, dependent: :destroy
	belongs_to :cidade

end
