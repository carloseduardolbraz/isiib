class Ministerio < ActiveRecord::Base
	has_many :ocupacao
	belongs_to :igreja
	validates_presence_of :ministerio, :message=>"Esté campo é obrigatório!"
	validates :ministerio, uniqueness: {scope: [:ministerio,:igreja_id] , :message=> "Este ministério já foi cadastrado!" }
	validates_presence_of :igreja_id, message: "Selecione uma igreja"
end
