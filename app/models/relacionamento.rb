class Relacionamento < ActiveRecord::Base
  belongs_to :membro
  belongs_to :parentesco, class_name: "Membro"
end
