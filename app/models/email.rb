class Email < ActiveRecord::Base  
	#Associações
	has_many :pessoa, through: :pessoa_email
	belongs_to :contato_email, :polymorphic => true

	#validações
	validates_presence_of :email, message: "Digite o email"

end
