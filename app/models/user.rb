class User < ActiveRecord::Base
	has_many :permissions, as: :grupousuario, :dependent => :destroy
	belongs_to :group
	has_many :UserIgrejas
	has_many :igrejas, through: :UserIgrejas
	has_secure_password
	
	validates :login, presence: true, length: {maximum: 50}
	validates :password, presence: false, length: {minimum: 6}
	#VALID_EMAIL_FORMAT= /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
	#validates :email, presence: true, length: {maximum: 260}, format: { with: VALID_EMAIL_FORMAT}, uniqueness: {case_sensitive: false}
	
	#before_save { self.email = email.downcase }

	def generate_auth_token
  		payload = { user_id: self.id }
  		AuthToken.encode(payload)
	end
	
	#  Autentica o usuário de acordo com seu login e sua senha
	#  e Cria o JWT para validar o Client que está fazendo os request
	#  Author::  Carlos Eduardo L. Braz <carloseduardo@lanwise.com.br>
	#  Param:: {Hash}  dados    Dados utilizados para fazer o login
	#  Return:: {AuthToken}  Token de acesso
	def logar(dados, time_token=nil)
		begin			
			@login = dados[:login]
			@password = dados[:password]

			usuario = User.where(login: @login).includes(:igrejas).where('igrejas.id'=>dados[:IgrejaId]).first
			unless usuario
				raise 'Usuario ou senha incorretos!'
			end
			usuario_autenticado = usuario.authenticate(@password).to_hash			
			
			usuario_autenticado = {:login=>usuario_autenticado["login"], :IgrejaId =>dados[:IgrejaId]}

			retorno = AuthToken.encode(usuario_autenticado)
			
			if !time_token.nil?
				retorno = AuthToken.encode(usuario_autenticado,time_token)
			end			
			retorno			

		rescue =>erro			
			puts erro.message
		end
	end
	
	#   Verifica se a instancia do usuário possui permissão a uma dada Action
	#   Author:: Carlos Eduardo L. Braz    <carloseduardo@lanwise.com.br>
	#   Param:: {String}  url/action que deseja verificar
	#   Return:: {Bool}	
	def authorized?(action)
		if self.permissions
			#pu = Permissão de Usuario
			self.permissions.each do |pu|
				if pu.action.name == action && pu.level
					return true
				end
			end
		end

		if self.group.permissions
			#pg = Permissão de Grupo
			self.group.permissions.each do |pg|
				if pg.action.name == action && pg.level
					return true
				end
			end
		end

		false
	end
	


	# Retorna Array com lista de permissões
	# Author::Carlos Eduardo L. Braz   <carloseduardo@lanwise.com.br>
	# Return:: {Hash}  result	
	def permissionsView
		result ||= []  
		unless self.permissions.blank?
			self.permissions.each do |p|
				#verifica se existe description para o Acos
				#Assim distingue se o Aco/Action é uma view no cliente ou não
				if p.action.aco.description.present? && p.action.description.to_s.semAcentuacao.present?
					result << p.action.aco.description.to_s.semAcentuacao + '/' + p.action.description.to_s.semAcentuacao
				end
			end
		end		
		Base64.encode64 result.to_json
	end

	# Converte o Objeto User em um Hash para criptografar no AuthToken
	#
	# Author:: Carlos Eduardo Lima Braz    <carloseduardo@lanwise.com.br>
	#
	def to_hash
	    hash = {}; self.attributes.each { |k,v| hash[k] = v }
    return hash
  end
end



