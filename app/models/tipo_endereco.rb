class TipoEndereco < ActiveRecord::Base
	has_many :enderecos
  validates_presence_of :tipo, message: "Tipo de Endereço é obrigatório"
end
