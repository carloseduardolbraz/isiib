class Telefone < ActiveRecord::Base
	#Associações
	has_many :configuracaos	
	has_many :pessoa, through: :pessoa_telefone
    has_many :observacaos, as: :observacaomodel	

  	validates_presence_of :numero, message: "Número de telefone é obrigatório"	
	belongs_to :contato_telefone, :polymorphic => true
end
