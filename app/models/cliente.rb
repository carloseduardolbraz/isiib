class Cliente < ActiveRecord::Base
	#Associações
	has_many :igrejas, :dependent => :destroy
	has_many :tipo_documentos, :dependent => :destroy
	has_many :situacaos, :dependent => :destroy
	has_many :profissaos, :dependent => :destroy
	has_many :grau_instrucao, :dependent => :destroy
	has_many :configuracaos, :dependent => :destroy
	has_many :tipo_telefone, :dependent => :destroy
	has_many :groups, :dependent => :destroy
	has_one  :configuracao, :dependent => :destroy
end
