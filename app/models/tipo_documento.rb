class TipoDocumento < ActiveRecord::Base
  has_many :documentos
  belongs_to :cliente
  validates_presence_of :tipo, message: "O campo Tipo do Documento é obrigatório"
end
