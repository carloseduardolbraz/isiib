class Group < ActiveRecord::Base
	has_many :permissions, as: :grupousuario
	has_many  :user_igreja
	belongs_to :cliente

	validates :name, presence: true
end


