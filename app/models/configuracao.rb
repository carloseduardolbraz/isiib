class Configuracao < ActiveRecord::Base
	belongs_to :cliente
	validates_presence_of :cliente_id, message: "O Cliente não foi selecionado"
end
