class UserIgreja < ActiveRecord::Base
	has_many :permissions, as: :grupousuario
	belongs_to :igreja
	belongs_to :user
	belongs_to :group
end
