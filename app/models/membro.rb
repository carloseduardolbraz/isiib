class Membro < ActiveRecord::Base
  has_many :observacaos, as: :observacaomodel
  has_many :membro_ocupacaos
  has_many :ocupacaos, through: :membro_ocupacaos

  
  has_one :situacao
  has_one :profissao
  belongs_to :forma_transicao
  
  belongs_to :pessoa
end
