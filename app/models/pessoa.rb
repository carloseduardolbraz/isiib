class Pessoa < ActiveRecord::Base
  has_many :relacionamentos
  has_many :parentesco, through: :relacionamentos, source: :parentesco  
  has_many :documentos, :dependent => :destroy
  
  has_one :membro, :dependent => :destroy
  #HACK PARA O POLYMORPHIC
  has_many :enderecos, as: :localizacao, :dependent => :destroy
  has_many :telefones, as: :contato_telefone, :dependent => :destroy
  has_many :emails, as: :contato_email, :dependent => :destroy
  

  #validações
  validates_presence_of :nome, message: "Campo Nome é obrigatório"
  validates_presence_of :sexo, message: "Selecione o sexo."      
end
