class Profissao < ActiveRecord::Base
	has_many 	:membros
	belongs_to	:cliente
	
  	validates_presence_of :profissao, message: "Por favor, informe a profissão que deseja cadastra."
end
