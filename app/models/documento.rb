class Documento < ActiveRecord::Base
	#Associações
	belongs_to :pessoa, dependent: :destroy
	has_one :tipo_documentos
	
	#método para validar a data	
	def valida_data_validade
	  errors.add(:data_validade,
	      "Data da validade do documento não pode ser maior que a Data de hoje") if
	      data_validade > Time.now
	end
end
