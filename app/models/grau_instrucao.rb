class GrauInstrucao < ActiveRecord::Base
	has_many :clientes
	has_many :pessoas
    validates_presence_of :descricao, message: "Descrição do Grau de Instrução é obrigatório"
end
