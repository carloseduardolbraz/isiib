class Situacao < ActiveRecord::Base
	has_many 	:membros
	belongs_to	:cliente
	
  	validates_presence_of :descricao, message: "O Campo descrição é obrigatório "
  	validates :descricao, uniqueness: { scope: [:descricao, :cliente_id], message: "Situação já foi cadastrado" }
end
