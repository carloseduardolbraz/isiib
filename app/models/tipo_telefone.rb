class TipoTelefone < ActiveRecord::Base
	has_many :telefones
	belongs_to :clientes
  	
  	validates_presence_of :tipo, message: "Tipo de Telefone é obrigatório"
end
