class Cidade < ActiveRecord::Base
	has_many :bairros, dependent: :destroy
	belongs_to :estado
end
