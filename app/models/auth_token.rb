require 'jwt'
require 'decode_auth_token.rb'
class AuthToken	

	#  Construtor da classe
	# Author:: Carlos Eduardo L. Braz   <carloseduard@lanwise.com.br>
	# Param:: {String} token  Token já criptografado (self.encode)
	#
	def initialize(token)
		if token
			@payload =AuthToken.decode(token)
		end
	end



	# Método Estático para criptografar o JWT de acordo com o Hash
	# Author:: Carlos Eduardo L. Braz   <carloseduard@lanwise.com.br>
	# Param:: {Hash}   payload    Informações que transitarão na Rede (Client)  
	#
	def self.encode(payload, time_token=30.minute.from_now)
		#tempo de duração da sessão
		payload[:time_token] = time_token
		JWT.encode(payload, Rails.application.secrets.secret_key_base)
	end		

	#  Descriptografa o Token do JWT e verifica se a sessão do token ainda é válida
	#  Author:: Carlos Eduardo L. Braz   <carloseduardo@lanwise.com.br>
	#  Param:: {JWT}  JSON Wep Token
	#  Return: Bool
	def self.decode(token)		
		payload = JWT.decode(token, Rails.application.secrets.secret_key_base)[0]				
  	end  



  	#  Método Get para retornar o Payload
  	#  Author:: Carlos Eduardo L. Braz     <carloseduardo@lanwise.com.br>
  	#  Param:: void
  	#  Return:: {Hash}  @payload
  	def payload
  		@payload
  	end

  	#  Setter do time_token
  	#  Author:: Carlos Eduardo L. Braz   <carloseduardo@lanwise.com.br>
  	#  Param:: {ActiveSupport::TimeWithZone} time
  	#  Return:: {String}  JWT
  	def time_token=(time)
  		@payload[:time_token] = time
  		AuthToken.encode(@payload)
	end

end