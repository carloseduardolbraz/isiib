class Igreja < ActiveRecord::Base
	
	belongs_to :cliente, dependent: :destroy
	belongs_to :configuracao, dependent: :destroy
	has_many :pessoas, dependent: :destroy
	has_many :enderecos, as: :localizacao
	has_many :telefones, as: :contato_telefone
	has_many :emails, as: :contato_email

	has_many :igrejas, foreign_key: "igreja_id"
	has_many :tipo_assembleias

 	has_many :UserIgrejas
    has_many :users, through: :UserIgrejas
  	validates_presence_of :razao_social, message: "Razão Social deve ser preenchida"

  	##
	# Retorna qual é a igreja mãe de uma congregação
	#
	# caso essa igreja não seja uma congregação (não possui o igreja_id)
	# first pointed is copied and placed at the end of the line.
	#
	# An ArgumentError is raised if the line crosses itself, but shapes may
	# be concave.# Cria um   	
  	#
  	def igreja_mae
  		unless self.igreja_id
  			return []
  		end
  		Igreja.find(self.igreja_id)
  	end


end
