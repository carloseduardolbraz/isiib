class Endereco < ActiveRecord::Base
	#Associações
	belongs_to :bairro
	belongs_to :pessoa
	has_one :tipo_endereco
	has_one :configuracaos
	belongs_to :localizacao, :polymorphic => true
end
