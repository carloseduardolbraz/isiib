json.array!(@bairros) do |bairro|
	json.extract! bairro, :id, :bairro
	json.url bairro_url(bairro, format: :json)
end