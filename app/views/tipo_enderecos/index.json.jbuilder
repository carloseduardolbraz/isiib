json.array!(@tipoEnderecos) do |tipo_endereco|
  json.extract! tipo_endereco, :id, :tipo
  json.url tipo_endereco_url(tipo_endereco, format: :json)
end
