igreja = @igreja
json.igreja(igreja, :id, :razao_social, :nome_fantasia, :cnpj, :created_at, :updated_at)
if @congregacao == 'true'
	json.congregacoes igreja.igrejas do |congregacao|
		json.igreja(congregacao, :id, :razao_social, :nome_fantasia, :cnpj, :created_at, :updated_at) 	
		json.enderecos congregacao.enderecos do |endereco|
		 	json.(endereco, :logradouro, :cep, :numero, :complemento) 
		end
		json.telefones congregacao.telefones do |telefone|
		 	json.(telefone, :numero,:ramal) 
		end
		json.emails congregacao.emails do |email|
		 	json.(email, :email) 
		end
	end
end
#Cria o JSON de enderecos
json.enderecos igreja.enderecos do |endereco|
 	json.(endereco, :logradouro, :cep, :numero, :complemento) 
end
#Cria o JSON de telefones
json.telefones igreja.telefones do |telefone|
 	json.(telefone, :numero)
 	json.(telefone.observacaos[0], :observacao) 

end
#Cria o JSON de emails
json.emails igreja.emails do |email|
 	json.(email, :email) 
end