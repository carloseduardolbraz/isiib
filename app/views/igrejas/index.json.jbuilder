json.array!(@igrejas) do |igreja|
 	json.igreja(igreja, :id, :razao_social, :nome_fantasia, :cnpj, :created_at, :updated_at)
	#Cria o JSON de enderecos
	json.enderecos igreja.enderecos do |endereco|
	 	json.(endereco, :logradouro, :cep, :numero, :complemento) 
	end
	
	#Cria o JSON de telefones
	json.telefones igreja.telefones do |telefone|
	 	json.(telefone, :numero,:ramal) 
	end
	
	#Cria o JSON de emails
	json.emails igreja.emails do |email|
	 	json.(email, :email) 
	end
	json.url igreja_url(igreja, format: :json)
end
