json.array!(@grau_instrucaos) do |grau_instrucao|
  json.extract! grau_instrucao, :id, :descricao
  json.url grau_instrucao_url(grau_instrucao, format: :json)
end
