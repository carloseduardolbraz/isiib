#Retorna o array de Membros
json.array!(@membros) do |membro|
	json.membro(membro, :data_batismo, :data_inativacao)
	#Verifico se está vinculado a algum membro
	if membro.pessoa
		json.pessoa(membro.pessoa, :nome, :forma_tratamento, :sexo, :cpf, :foto, :nacionalidade, :naturalidade)
		#Hack pra alterar a data dentro do symbolo
		json.set! :pessoa do
		  json.set! :data_nascimento, l(membro.pessoa[:data_nascimento])
		end
		
		#Cria o JSON de enderecos
		json.enderecos membro.pessoa.enderecos do |endereco|
		 	json.(endereco, :logradouro, :cep, :numero, :complemento) 
		end
		#Cria o JSON de emails
		json.emails membro.pessoa.emails do |email|
		 	json.(email, :email) 
		end
		#Cria o JSON de telefones
		json.telefones membro.pessoa.telefones do |telefone|
		 	json.(telefone, :ramal, :numero) 
		end
	else
		json.null
	end
end
