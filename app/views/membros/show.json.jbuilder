membro = @membro
json.membro(membro, :data_batismo, :data_inativacao)
json.set! :membro do
	if membro[:data_batismo]
		json.set! :data_batismo, l(membro[:data_batismo])
	end
	if membro[:data_inativacao]
		json.set! :data_inativacao, l(membro[:data_inativacao])
	end
end
#Verifico se está vinculado a algum membro
if membro.pessoa
	json.pessoa(membro.pessoa, :nome, :forma_tratamento, :sexo, :cpf, :foto, :nacionalidade, :naturalidade)
	#Hack pra alterar a data dentro do symbolo
	json.set! :pessoa do
	  json.set! :data_nascimento, membro.pessoa[:data_nascimento].to_s('%d/%m/%Y')
	end
	#Cria o JSON de enderecos
	json.enderecos membro.pessoa.enderecos do |endereco|
	 	json.(endereco, :logradouro, :cep) 
	end
	#Cria o JSON de emails
	json.emails membro.pessoa.emails do |email|
	 	json.(email, :email) 
	end
	#Cria o JSON de telefones
	json.telefones membro.pessoa.telefones do |telefone|
	 	json.(telefone, :ramal, :numero) 
	end
else
	json.null
end