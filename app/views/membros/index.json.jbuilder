#Retorna o array de Membros
json.data do 
	json.array!(@membros) do |membro|
		json.membro(membro, :data_batismo, :data_inativacao)
		json.set! :membro do
			if membro[:data_batismo]
				json.set! :data_batismo, l(membro[:data_batismo])
			end
			if membro[:data_inativacao]
				json.set! :data_inativacao, l(membro[:data_inativacao])
			end
		end
		#Verifico se está vinculado a algum membro
		if membro.pessoa
			json.pessoa(membro.pessoa, :nome, :forma_tratamento, :sexo, :cpf, :foto, :nacionalidade, :naturalidade)
			
			#Hack pra alterar a data dentro do symbolo
			if membro.pessoa[:data_nascimento]		
				json.set! :data_nascimento, l(membro.pessoa[:data_nascimento])
			else
				json.set! :data_nascimento, nil
			end
			#Cria o JSON de enderecos
			json.enderecos membro.pessoa.enderecos do |endereco|
			 	json.(endereco, :logradouro, :cep, :numero, :complemento) 
			end
			#Cria o JSON de emails
			json.emails membro.pessoa.emails do |email|
			 	json.(email, :email) 
			end
			#Cria o JSON de telefones
			json.telefones membro.pessoa.telefones do |telefone|
			 	json.(telefone, :ramal, :numero) 
			end
		else
			json.null
		end
	end
end
if defined? @membros.current_page
	json.paginate( :total => @membros.total_entries, :current_page => @membros.current_page, :total_pages => @membros.total_pages, :next_page=> @membros.next_page,	:previous_page => @membros.previous_page)
end