json.array!(@ministerios) do |ministerio|
  json.extract! ministerio, :id, :ministerio
  json.url ministerio_url(ministerio, format: :json)
end
