json.array!(@pessoas) do |pessoa|
  json.extract! pessoa, :id, :nome, :data_nascimento, :sexo, :cpf, :foto, :nacionalidade, :naturalidade
  json.set! :data_nascimento, l(pessoa[:data_nascimento])
	

  json.enderecos pessoa.enderecos do |endereco|
  	json.(endereco, :logradouro, :cep, :numero, :complemento) 
  end

  json.emails pessoa.emails do |email|
  	json.(email, :email)
  end

  json.telefones pessoa.telefones do |telefone|
  	json.(telefone, :numero)
  end

end