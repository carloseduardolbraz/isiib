json.array!(@acos) do |aco|
  json.extract! aco, :id, :name, :description, :actions
  json.url aco_url(aco, format: :json)
end
