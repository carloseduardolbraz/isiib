json.array!(@actions) do |action|
  json.extract! action, :id, :name, :aco, :description
  json.url action_url(action, format: :json)
end
