json.array!(@profissaos) do |profissao|
  json.extract! profissao, :id, :profissao
  json.url profissao_url(profissao, format: :json)
end
