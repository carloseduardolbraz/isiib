json.array!(@documentos) do |documento|
  json.extract! documento, :id, :documento, :orgao_expeditor, :data_expedicao, :data_validade
  json.url documento_url(documento, format: :json)
end
