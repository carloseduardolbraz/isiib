# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
def set_cliente
	if Cliente.present?
		return
	end
	increment = Cliente.count
	4.times do
		increment = increment + 1
		c = Cliente.create id: increment, data_cadastro: DateTime.now
		c.configuracao = Configuracao.new configurado: true
		c.save
	end
end
puts "Clientes Cadastrads com sucesso" if set_cliente


def set_tipo_endereco
	if TipoEndereco.present?
		return
	end
	# Pré configurar os tipos de endereço
	['Residencial','Comercial'].each do |tipo|
		TipoEndereco.create(tipo: tipo)
end
puts 'Tipo de Endereço carregados com sucesso' if set_tipo_endereco



def set_igreja
	if Igreja.present?
		return
	end
	increment = Igreja.count
		5.times do
			increment = increment + 1
			i = Igreja.new(id: increment, razao_social: 'Igreja Batistas ' + Faker::Name.name ,nome_fantasia: Faker::Name.first_name, cnpj: Faker::Number.number(14))
			i.Cliente = Cliente.find increment
			i.save
		end
end
puts 'Igrejas carregadas com sucesso' if set_tipo_endereco


def set_grau_instrucao
	if GrauInstrucao.present?
		return
	end
	# Pré configurar os grau de instrucao
	['Analfabeto','Ensino Fundamental','2º grau incompleto','2º grau completo','Superior Incompleto','Superior Completo','Pós-Graduação/Especialização','Mestrado','Doutorado','Pós-Doutorado'].each do |descricao|
		GrauInstrucao.create(descricao: descricao)
	end
end
puts 'Grau de Instrucao carregados com sucesso' if set_grau_instrucao


def set_estados
	if Estado.present?
		return
	end
	estados = File.open("#{Rails.root}/db/estado.json", 'r').read
	estados =  JSON.parse estados
	estados.each do |estado|
		Estado.create(estado) unless Estado.exists? estado['id']
	end
	return true
end
puts "Estados carregados com sucesso!" if set_estados

def set_cidades
	if Cidade.present?
		return
	end
	cidades = File.open("#{Rails.root}/db/cidades.json",'r').read
	cidades = JSON.parse cidades
	cidades.each do |cidade|

		Cidade.create(cidade) unless Cidade.exists? cidade['id']
	end
	return true
end
puts "Cidades Carregadas com sucess!" if set_cidades


def set_situacoes
	if Situacao.present?
		return
	end
		["Ativo","Inativo","Visitante","Afastado"].each do |s|
			Situacao.create(descricao: s)
		end
end

puts "Situções carregadas com sucesso" if set_situacoes

def set_forma_transicao
	if FormaTransicao.present?
		return
	end
	["Batismo","Aclamação","Transferência"].each do |ingresso|
		FormaTransicao.create descricao: ingresso, tipo: 'I'
	end
end
puts "Formas de Transição carregadas com sucesso" if  set_forma_transicao


def set_ministerios
	if Ministerio.present?
		return
	end
	["Ministério de Louvor","Ministério Infantil","Grupo de Evangelismo"].each do |ministerio|
		m = Ministerio.create ministerio: ministerio
		m.igreja = Igreja.first
		m.save
	end
end
