class CreatePermissions < ActiveRecord::Migration
  def change
    create_table :permissions do |t|
      t.integer :level
      #Comportamento de Polymorphic = >
      t.integer :grupousuario_id
      t.string :grupousuario_type

      t.timestamps null: false
    end
  end
end

