class AddBairroRefToEnderecos < ActiveRecord::Migration
  def change
    add_reference :enderecos, :bairro, index: true
    add_foreign_key :enderecos, :bairros
  end
end
