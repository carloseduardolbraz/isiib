class CreateConfiguracaos < ActiveRecord::Migration
  def change
    create_table :configuracaos do |t|
      t.string :logo
      t.string :configurado, :limit => 1
      t.timestamps null: false
    end
  end
end
