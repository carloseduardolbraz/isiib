class AddPessoaRefToGrauInstrucaos < ActiveRecord::Migration
  def change
    add_reference :grau_instrucaos, :pessoa, index: true
    add_foreign_key :grau_instrucaos, :pessoas
  end
end
