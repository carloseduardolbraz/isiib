class AddMembroToMembroOcupacao < ActiveRecord::Migration
  def change
    add_reference :membro_ocupacaos, :membro, index: true
    add_foreign_key :membro_ocupacaos, :membros
  end
end
