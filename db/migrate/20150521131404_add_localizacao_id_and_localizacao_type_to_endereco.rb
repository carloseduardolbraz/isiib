class AddLocalizacaoIdAndLocalizacaoTypeToEndereco < ActiveRecord::Migration
  def change
    add_column :enderecos, :localizacao_id, :integer
    add_column :enderecos, :localizacao_type, :string
  end
end
