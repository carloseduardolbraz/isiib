class AddEnderecoRefToConfiguracaos < ActiveRecord::Migration
  def change
    add_reference :configuracaos, :endereco, index: true
    add_foreign_key :configuracaos, :enderecos
  end
end
