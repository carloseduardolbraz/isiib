class AddConfiguradoToCliente < ActiveRecord::Migration
  def change
    add_column :clientes, :configurado, :string, :limit => 1
  end
end
