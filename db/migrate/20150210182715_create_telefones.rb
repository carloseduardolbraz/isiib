class CreateTelefones < ActiveRecord::Migration
  def change
    create_table :telefones do |t|
      t.string :numero, limit: 20, null: false
      t.integer :ramal

      t.timestamps null: false
    end
  end
end
