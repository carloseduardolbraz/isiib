class AddClienteRefToIgreja < ActiveRecord::Migration
  def change
    add_reference :igrejas, :cliente, index: true
    add_foreign_key :igrejas, :clientes
  end
end
