class AddClienteRefToSituacaos < ActiveRecord::Migration
  def change
    add_reference :situacaos, :cliente, index: true
    add_foreign_key :situacaos, :clientes
  end
end
