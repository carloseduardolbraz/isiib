class AddIgrejaToTipoAssembleia < ActiveRecord::Migration
  def change
    add_reference :tipo_assembleia, :igreja, index: true
    add_foreign_key :tipo_assembleia, :igrejas
  end
end
