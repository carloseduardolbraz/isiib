class AddSituacaoToMembro < ActiveRecord::Migration
  def change
    add_reference :membros, :situacao, index: true
    add_foreign_key :membros, :situacaos
  end
end
