class CreateEnderecos < ActiveRecord::Migration
  def change
    create_table :enderecos do |t|
      t.string :logradouro, limit: 200
      t.string :cep, limit: 12
      t.string :numero, limit: 10
      t.string :complemento, limit: 250

      t.timestamps null: false
    end
  end
end
