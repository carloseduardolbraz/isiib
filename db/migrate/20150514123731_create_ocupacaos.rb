class CreateOcupacaos < ActiveRecord::Migration
  def change
    create_table :ocupacaos do |t|
      t.string :ocupacao, :limit=>50
      t.timestamps null: false
    end
  end
end
