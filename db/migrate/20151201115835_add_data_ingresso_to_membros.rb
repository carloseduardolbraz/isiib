class AddDataIngressoToMembros < ActiveRecord::Migration
  def change
    add_column :membros, :data_ingresso, :date
  end
end
