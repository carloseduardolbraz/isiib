class AddContatoIdAndContatoTypeToEmail < ActiveRecord::Migration
  def change
  	add_column :emails, :contato_email_id, :integer
    add_column :emails, :contato_email_type, :string
  end
end
