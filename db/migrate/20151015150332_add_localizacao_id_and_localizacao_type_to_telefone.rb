class AddLocalizacaoIdAndLocalizacaoTypeToTelefone < ActiveRecord::Migration
  def change
  	add_column :telefones, :contato_telefone_id, :integer
    add_column :telefones, :contato_telefone_type, :string
  end
end
