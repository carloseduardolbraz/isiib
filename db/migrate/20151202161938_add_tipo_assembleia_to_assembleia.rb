class AddTipoAssembleiaToAssembleia < ActiveRecord::Migration
  def change
    add_reference :assembleia, :tipo_assembleia, index: true
    add_foreign_key :assembleia, :tipo_assembleia, column: :tipo_assembleia_id
  end
end
