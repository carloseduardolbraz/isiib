class CreateObservacaos < ActiveRecord::Migration
  def change
    create_table :observacaos do |t|
      t.string :observacao

      # Comportamento do Polimorphic
      t.integer :observacaomodel_id
      t.string :observacaomodel_type

      t.timestamps null: false
    end
  end
end
