class AddSituacaoToPessoa < ActiveRecord::Migration
  def change
    add_reference :pessoas, :situacao, index: true
    add_foreign_key :pessoas, :situacaos
  end
end
