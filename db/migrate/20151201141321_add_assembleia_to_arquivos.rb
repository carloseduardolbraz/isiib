class AddAssembleiaToArquivos < ActiveRecord::Migration
  def change
    add_reference :arquivos, :assembleia, index: true
    add_foreign_key :arquivos, :assembleia, column: :assembleia_id
  end
end
