class AddPessoaToDocumentos < ActiveRecord::Migration
  def change
    add_reference :documentos, :pessoa, index: true
    add_foreign_key :documentos, :pessoas
  end
end
