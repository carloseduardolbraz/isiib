class AddGroupToUserIgreja < ActiveRecord::Migration
  def change
    add_reference :user_igrejas, :group, index: true
    add_foreign_key :user_igrejas, :groups
  end
end
