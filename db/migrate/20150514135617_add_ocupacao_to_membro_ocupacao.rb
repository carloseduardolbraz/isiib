class AddOcupacaoToMembroOcupacao < ActiveRecord::Migration
  def change
    add_reference :membro_ocupacaos, :ocupacao, index: true
    add_foreign_key :membro_ocupacaos, :ocupacaos
  end
end
