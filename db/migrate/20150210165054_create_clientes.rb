class CreateClientes < ActiveRecord::Migration
  def change
    create_table :clientes do |t|
      t.timestamp :data_cadastro
      t.timestamp :data_inicio_contrato
      t.timestamp :data_fim_contrato

      t.timestamps null: false
    end
  end
end
