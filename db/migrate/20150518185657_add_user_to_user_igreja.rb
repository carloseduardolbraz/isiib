class AddUserToUserIgreja < ActiveRecord::Migration
  def change
    add_reference :user_igrejas, :user, index: true
    add_foreign_key :user_igrejas, :users
  end
end
