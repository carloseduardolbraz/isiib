class AddFormaTransicaoToMembros < ActiveRecord::Migration
  def change
    add_reference :membros, :forma_transicao, index: true
    add_foreign_key :membros, :forma_transicaos
  end
end
