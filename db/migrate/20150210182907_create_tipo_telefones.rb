class CreateTipoTelefones < ActiveRecord::Migration
  def change
    create_table :tipo_telefones do |t|
      t.string :tipo, limit: 20,null: false

      t.timestamps null: false
    end
  end
end
