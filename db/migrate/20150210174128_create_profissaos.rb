class CreateProfissaos < ActiveRecord::Migration
  def change
    create_table :profissaos do |t|
      t.string :profissao, limit: 100,null:false, uniqueness:true

      t.timestamps null: false
    end
  end
end
