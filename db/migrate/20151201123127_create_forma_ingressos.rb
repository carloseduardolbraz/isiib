class CreateFormaIngressos < ActiveRecord::Migration
  def change
    create_table :forma_ingressos do |t|
      t.string :descricao
      t.string :tipo , :limit => 1
      t.timestamps null: false
    end
  end
end
