class AddPessoaToPessoaEmail < ActiveRecord::Migration
  def change
    add_reference :pessoa_emails, :pessoa, index: true
    add_foreign_key :pessoa_emails, :pessoas
  end
end
