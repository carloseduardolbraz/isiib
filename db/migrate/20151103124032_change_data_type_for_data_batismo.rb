class ChangeDataTypeForDataBatismo < ActiveRecord::Migration
  def change
  	change_table :membros do |t|
      t.change :data_batismo, :datetime
    end
  end
end
