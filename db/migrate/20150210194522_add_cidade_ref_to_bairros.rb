class AddCidadeRefToBairros < ActiveRecord::Migration
  def change
    add_reference :bairros, :cidade, index: true
    add_foreign_key :bairros, :cidades
  end
end
