class CreateSituacaos < ActiveRecord::Migration
  def change
    create_table :situacaos do |t|
      t.string :descricao, limit:100, uniqueness:true, null:false

      t.timestamps null: false
    end
  end
end
