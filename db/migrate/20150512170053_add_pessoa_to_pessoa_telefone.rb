class AddPessoaToPessoaTelefone < ActiveRecord::Migration
  def change
    add_reference :pessoa_telefones, :pessoa, index: true
    add_foreign_key :pessoa_telefones, :pessoas
  end
end
