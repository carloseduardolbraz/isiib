class AddPessoaToMembro < ActiveRecord::Migration
  def change
    add_reference :membros, :pessoa, index: true
    add_foreign_key :membros, :pessoas
  end
end
