class AddNaturalidadeToPessoa < ActiveRecord::Migration
  def change
    add_column :pessoas, :naturalidade, :string, :limit=>50
  end
end
