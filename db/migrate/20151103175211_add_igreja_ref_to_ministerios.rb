class AddIgrejaRefToMinisterios < ActiveRecord::Migration
  def change
    add_reference :ministerios, :igreja, index: true
    add_foreign_key :ministerios, :igrejas
  end
end
