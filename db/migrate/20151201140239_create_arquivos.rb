class CreateArquivos < ActiveRecord::Migration
  def change
    create_table :arquivos do |t|
      t.string :url
      t.timestamps null: false
    end
  end
end
