class AddProfissaoToMembro < ActiveRecord::Migration
  def change
    add_reference :membros, :profissao, index: true
    add_foreign_key :membros, :profissaos
  end
end
