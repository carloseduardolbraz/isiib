class AddIgrejaRefToPessoas < ActiveRecord::Migration
  def change
    add_reference :pessoas, :igreja, index: true
    add_foreign_key :pessoas, :igrejas
  end
end
