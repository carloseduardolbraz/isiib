class AddEstadoRefToCidades < ActiveRecord::Migration
  def change
    add_reference :cidades, :estado, index: true
    add_foreign_key :cidades, :estados
  end
end
