class CreateMinisterios < ActiveRecord::Migration
  def change
    create_table :ministerios do |t|
      t.string :ministerio
      t.timestamps null: false
    end
  end
end
