class AddClienteRefToConfiguracaos < ActiveRecord::Migration
  def change
    add_reference :configuracaos, :cliente, index: true
    add_foreign_key :configuracaos, :clientes
  end
end
