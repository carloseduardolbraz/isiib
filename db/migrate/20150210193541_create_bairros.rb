class CreateBairros < ActiveRecord::Migration
  def change
    create_table :bairros do |t|
      t.string :bairro, null: false, uniqueness: true

      t.timestamps null: false
    end
  end
end
