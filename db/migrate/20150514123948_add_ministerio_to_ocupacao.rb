class AddMinisterioToOcupacao < ActiveRecord::Migration
  def change
    add_reference :ocupacaos, :ministerio, index: true
    add_foreign_key :ocupacaos, :ministerios
  end
end
