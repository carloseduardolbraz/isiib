class AddIgrejaToUserIgreja < ActiveRecord::Migration
  def change
    add_reference :user_igrejas, :igreja, index: true
    add_foreign_key :user_igrejas, :igrejas
  end
end
