class AddClienteRefToProfissaos < ActiveRecord::Migration
  def change
    add_reference :profissaos, :cliente, index: true
    add_foreign_key :profissaos, :clientes
  end
end
