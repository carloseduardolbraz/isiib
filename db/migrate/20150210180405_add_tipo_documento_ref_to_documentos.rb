class AddTipoDocumentoRefToDocumentos < ActiveRecord::Migration
  def change
    add_reference :documentos, :tipo_documento, index: true
    add_foreign_key :documentos, :tipo_documentos
  end
end
