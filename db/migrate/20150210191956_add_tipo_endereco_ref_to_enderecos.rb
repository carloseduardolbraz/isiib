class AddTipoEnderecoRefToEnderecos < ActiveRecord::Migration
  def change
    add_reference :enderecos, :tipo_endereco, index: true
    add_foreign_key :enderecos, :tipo_enderecos
  end
end
