class CreateTipoDocumentos < ActiveRecord::Migration
  def change
    create_table :tipo_documentos do |t|
      t.string :tipo, limit: 50, null: false

      t.timestamps null: false
    end
  end
end
