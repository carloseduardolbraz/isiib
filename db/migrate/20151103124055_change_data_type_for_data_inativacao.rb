class ChangeDataTypeForDataInativacao < ActiveRecord::Migration
  def change
  	change_table :membros do |t|
      t.change :data_inativacao, :datetime
    end
  end
end
