class CreateIgrejas < ActiveRecord::Migration
  def change
    create_table :igrejas do |t|
      t.string :razao_social, null:false
      t.string :nome_fantasia
      t.string :cnpj, limit:14

      t.timestamps null: false
    end
  end
end
