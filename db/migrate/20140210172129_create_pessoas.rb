class CreatePessoas < ActiveRecord::Migration
  def change
    create_table :pessoas do |t|
      t.string :nome, null: false
      t.string :forma_tratamento
      t.timestamp :data_nascimento
      t.string :sexo, limit: 1 ,null: false
      t.string :cpf, limit: 11
      t.string :foto, limit: 250

      t.timestamps null: false
    end
  end
end
