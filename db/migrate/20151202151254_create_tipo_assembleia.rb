class CreateTipoAssembleia < ActiveRecord::Migration
  def change
    create_table :tipo_assembleia do |t|
      t.string :tipo_assembleia, null: false
      t.timestamps null: false
    end
  end
end
