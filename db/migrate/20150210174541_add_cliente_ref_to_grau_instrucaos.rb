class AddClienteRefToGrauInstrucaos < ActiveRecord::Migration
  def change
    add_reference :grau_instrucaos, :cliente, index: true
    add_foreign_key :grau_instrucaos, :clientes
  end
end
