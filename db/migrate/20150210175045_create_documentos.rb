class CreateDocumentos < ActiveRecord::Migration
  def change
    create_table :documentos do |t|
      t.string :documento, limit: 30,null: false
      t.string :orgao_expeditor, limit: 100
      t.timestamp :data_expedicao
      t.timestamp :data_validade, null:false

      t.timestamps null: false
    end
  end
end
