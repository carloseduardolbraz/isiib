class AddActionToPermissions < ActiveRecord::Migration
  def change
    add_reference :permissions, :action, index: true
    add_foreign_key :permissions, :actions
  end
end
