class AddTelefoneToPessoaTelefone < ActiveRecord::Migration
  def change
    add_reference :pessoa_telefones, :telefone, index: true
    add_foreign_key :pessoa_telefones, :telefones
  end
end
