class AddClienteRefToTipoTelefones < ActiveRecord::Migration
  def change
    add_reference :tipo_telefones, :cliente, index: true
    add_foreign_key :tipo_telefones, :clientes
  end
end
