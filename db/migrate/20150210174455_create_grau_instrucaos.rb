class CreateGrauInstrucaos < ActiveRecord::Migration
  def change
    create_table :grau_instrucaos do |t|
      t.string :descricao, limit: 100, uniqueness:true, null: false

      t.timestamps null: false
    end
  end
end
