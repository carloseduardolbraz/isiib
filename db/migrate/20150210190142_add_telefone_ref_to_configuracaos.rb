class AddTelefoneRefToConfiguracaos < ActiveRecord::Migration
  def change
    add_reference :configuracaos, :telefone, index: true
    add_foreign_key :configuracaos, :telefones
  end
end
