class CreateCidades < ActiveRecord::Migration
  def change
    create_table :cidades do |t|
      t.string :cidade, limit: 100, uniqueness: true, null: false

      t.timestamps null: false
    end
  end
end
