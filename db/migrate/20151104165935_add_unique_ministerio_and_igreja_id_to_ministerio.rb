class AddUniqueMinisterioAndIgrejaIdToMinisterio < ActiveRecord::Migration
  def change
  	add_index :ministerios, [:ministerio, :igreja_id], :unique => true
  end
end
