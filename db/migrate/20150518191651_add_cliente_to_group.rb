class AddClienteToGroup < ActiveRecord::Migration
  def change
    add_reference :groups, :cliente, index: true
    add_foreign_key :groups, :clientes
  end
end
