class CreateRelacionamentos < ActiveRecord::Migration
  def change
    create_table :relacionamentos do |t|
      t.integer :pessoa_id
      t.integer :pessoa_secundario_id
      t.timestamps null: false
    end
  end
end
