class AddEmailToPessoaEmail < ActiveRecord::Migration
  def change
    add_reference :pessoa_emails, :email, index: true
    add_foreign_key :pessoa_emails, :emails
  end
end
