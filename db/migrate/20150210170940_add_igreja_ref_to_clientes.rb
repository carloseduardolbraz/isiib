class AddIgrejaRefToClientes < ActiveRecord::Migration
  def change
    add_reference :clientes, :igreja, index: true
    add_foreign_key :clientes, :igrejas
  end
end
