class AddAcoToActions < ActiveRecord::Migration
  def change
    add_reference :actions, :aco, index: true
    add_foreign_key :actions, :acos
  end
end
