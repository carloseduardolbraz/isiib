class AddDescricaoToFormaTransicao < ActiveRecord::Migration
  def change
    add_column :forma_transicaos, :descricao, :string
    add_column :forma_transicaos, :tipo, :string
  end
end
