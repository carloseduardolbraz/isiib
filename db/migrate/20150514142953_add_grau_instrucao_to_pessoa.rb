class AddGrauInstrucaoToPessoa < ActiveRecord::Migration
  def change
    add_reference :pessoas, :grau_instrucao, index: true
    add_foreign_key :pessoas, :grau_instrucaos
  end
end
