class AddUniqueDescricaoAndClienteIdToSituacao < ActiveRecord::Migration
  def change
  	  	add_index :situacaos, [:descricao, :cliente_id], :unique => true
  end
end
