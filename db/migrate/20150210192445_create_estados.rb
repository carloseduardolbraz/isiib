class CreateEstados < ActiveRecord::Migration
  def change
    create_table :estados do |t|
      t.string :estado, null:false, uniqueness: true
      
      t.timestamps null: false
    end
  end
end
