class CreateAssembleia < ActiveRecord::Migration
	def change
	    create_table :assembleia do |t|
	      t.integer :numero
	      t.date :data_assembleia
	      t.string :assunto
	      t.string :numero_ata
	      t.timestamps null: false
	    end
	end
end
