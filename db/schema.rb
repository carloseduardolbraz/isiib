# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151202192331) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "acos", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "description"
  end

  create_table "actions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "aco_id"
    t.string   "description"
  end

  add_index "actions", ["aco_id"], name: "index_actions_on_aco_id", using: :btree

  create_table "arquivos", force: :cascade do |t|
    t.string   "url"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "assembleia_id"
  end

  add_index "arquivos", ["assembleia_id"], name: "index_arquivos_on_assembleia_id", using: :btree

  create_table "assembleia", force: :cascade do |t|
    t.integer  "numero"
    t.date     "data_assembleia"
    t.string   "assunto"
    t.string   "numero_ata"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "tipo_assembleia_id"
  end

  add_index "assembleia", ["tipo_assembleia_id"], name: "index_assembleia_on_tipo_assembleia_id", using: :btree

  create_table "bairros", force: :cascade do |t|
    t.string   "bairro",     null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "cidade_id"
  end

  add_index "bairros", ["cidade_id"], name: "index_bairros_on_cidade_id", using: :btree

  create_table "cidades", force: :cascade do |t|
    t.string   "cidade",     limit: 100, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "estado_id"
  end

  add_index "cidades", ["estado_id"], name: "index_cidades_on_estado_id", using: :btree

  create_table "clientes", force: :cascade do |t|
    t.datetime "data_cadastro"
    t.datetime "data_inicio_contrato"
    t.datetime "data_fim_contrato"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "igreja_id"
    t.string   "configurado",          limit: 1
  end

  add_index "clientes", ["igreja_id"], name: "index_clientes_on_igreja_id", using: :btree

  create_table "configuracaos", force: :cascade do |t|
    t.string   "logo"
    t.string   "configurado", limit: 1
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "cliente_id"
  end

  add_index "configuracaos", ["cliente_id"], name: "index_configuracaos_on_cliente_id", using: :btree

  create_table "documentos", force: :cascade do |t|
    t.string   "documento",         limit: 30,  null: false
    t.string   "orgao_expeditor",   limit: 100
    t.datetime "data_expedicao"
    t.datetime "data_validade",                 null: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "tipo_documento_id"
    t.integer  "pessoa_id"
  end

  add_index "documentos", ["pessoa_id"], name: "index_documentos_on_pessoa_id", using: :btree
  add_index "documentos", ["tipo_documento_id"], name: "index_documentos_on_tipo_documento_id", using: :btree

  create_table "emails", force: :cascade do |t|
    t.string   "email",              limit: 150, null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "contato_email_id"
    t.string   "contato_email_type"
  end

  create_table "enderecos", force: :cascade do |t|
    t.string   "logradouro",       limit: 200
    t.string   "cep",              limit: 12
    t.string   "numero",           limit: 10
    t.string   "complemento",      limit: 250
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "tipo_endereco_id"
    t.integer  "bairro_id"
    t.integer  "localizacao_id"
    t.string   "localizacao_type"
  end

  add_index "enderecos", ["bairro_id"], name: "index_enderecos_on_bairro_id", using: :btree
  add_index "enderecos", ["tipo_endereco_id"], name: "index_enderecos_on_tipo_endereco_id", using: :btree

  create_table "estados", force: :cascade do |t|
    t.string   "estado",     null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "forma_ingressos", force: :cascade do |t|
    t.string   "descricao"
    t.string   "tipo",       limit: 1
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "forma_transicaos", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "descricao"
    t.string   "tipo"
  end

  create_table "grau_instrucaos", force: :cascade do |t|
    t.string   "descricao",  limit: 100, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "cliente_id"
    t.integer  "pessoa_id"
  end

  add_index "grau_instrucaos", ["cliente_id"], name: "index_grau_instrucaos_on_cliente_id", using: :btree
  add_index "grau_instrucaos", ["pessoa_id"], name: "index_grau_instrucaos_on_pessoa_id", using: :btree

  create_table "groups", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "cliente_id"
  end

  add_index "groups", ["cliente_id"], name: "index_groups_on_cliente_id", using: :btree

  create_table "igrejas", force: :cascade do |t|
    t.string   "razao_social",             null: false
    t.string   "nome_fantasia"
    t.string   "cnpj",          limit: 14
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "cliente_id"
    t.integer  "igreja_id"
  end

  add_index "igrejas", ["cliente_id"], name: "index_igrejas_on_cliente_id", using: :btree

  create_table "membro_ocupacaos", force: :cascade do |t|
    t.date    "data_inicio"
    t.integer "membro_id"
    t.integer "ocupacao_id"
  end

  add_index "membro_ocupacaos", ["membro_id"], name: "index_membro_ocupacaos_on_membro_id", using: :btree
  add_index "membro_ocupacaos", ["ocupacao_id"], name: "index_membro_ocupacaos_on_ocupacao_id", using: :btree

  create_table "membros", force: :cascade do |t|
    t.datetime "data_batismo"
    t.datetime "data_inativacao"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "situacao_id"
    t.integer  "profissao_id"
    t.integer  "pessoa_id"
    t.date     "data_ingresso"
    t.integer  "forma_transicao_id"
  end

  add_index "membros", ["forma_transicao_id"], name: "index_membros_on_forma_transicao_id", using: :btree
  add_index "membros", ["pessoa_id"], name: "index_membros_on_pessoa_id", using: :btree
  add_index "membros", ["profissao_id"], name: "index_membros_on_profissao_id", using: :btree
  add_index "membros", ["situacao_id"], name: "index_membros_on_situacao_id", using: :btree

  create_table "ministerios", force: :cascade do |t|
    t.string   "ministerio"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "igreja_id"
  end

  add_index "ministerios", ["igreja_id"], name: "index_ministerios_on_igreja_id", using: :btree
  add_index "ministerios", ["ministerio", "igreja_id"], name: "index_ministerios_on_ministerio_and_igreja_id", unique: true, using: :btree

  create_table "observacaos", force: :cascade do |t|
    t.string   "observacao"
    t.integer  "observacaomodel_id"
    t.string   "observacaomodel_type"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "ocupacaos", force: :cascade do |t|
    t.string   "ocupacao",      limit: 50
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "ministerio_id"
  end

  add_index "ocupacaos", ["ministerio_id"], name: "index_ocupacaos_on_ministerio_id", using: :btree

  create_table "permissions", force: :cascade do |t|
    t.integer  "level"
    t.integer  "grupousuario_id"
    t.string   "grupousuario_type"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "action_id"
  end

  add_index "permissions", ["action_id"], name: "index_permissions_on_action_id", using: :btree

  create_table "pessoa_emails", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "email_id"
    t.integer  "pessoa_id"
  end

  add_index "pessoa_emails", ["email_id"], name: "index_pessoa_emails_on_email_id", using: :btree
  add_index "pessoa_emails", ["pessoa_id"], name: "index_pessoa_emails_on_pessoa_id", using: :btree

  create_table "pessoa_telefones", force: :cascade do |t|
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "pessoa_id"
    t.integer  "telefone_id"
  end

  add_index "pessoa_telefones", ["pessoa_id"], name: "index_pessoa_telefones_on_pessoa_id", using: :btree
  add_index "pessoa_telefones", ["telefone_id"], name: "index_pessoa_telefones_on_telefone_id", using: :btree

  create_table "pessoas", force: :cascade do |t|
    t.string   "nome",                          null: false
    t.string   "forma_tratamento"
    t.datetime "data_nascimento"
    t.string   "sexo",              limit: 1,   null: false
    t.string   "cpf",               limit: 11
    t.string   "foto",              limit: 250
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "igreja_id"
    t.text     "nacionalidade"
    t.string   "naturalidade",      limit: 50
    t.integer  "situacao_id"
    t.integer  "grau_instrucao_id"
  end

  add_index "pessoas", ["grau_instrucao_id"], name: "index_pessoas_on_grau_instrucao_id", using: :btree
  add_index "pessoas", ["igreja_id"], name: "index_pessoas_on_igreja_id", using: :btree
  add_index "pessoas", ["situacao_id"], name: "index_pessoas_on_situacao_id", using: :btree

  create_table "profissaos", force: :cascade do |t|
    t.string   "profissao",  limit: 100, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "cliente_id"
  end

  add_index "profissaos", ["cliente_id"], name: "index_profissaos_on_cliente_id", using: :btree

  create_table "relacionamentos", force: :cascade do |t|
    t.integer  "pessoa_id"
    t.integer  "pessoa_secundario_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "situacaos", force: :cascade do |t|
    t.string   "descricao",  limit: 100, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "cliente_id"
  end

  add_index "situacaos", ["cliente_id"], name: "index_situacaos_on_cliente_id", using: :btree
  add_index "situacaos", ["descricao", "cliente_id"], name: "index_situacaos_on_descricao_and_cliente_id", unique: true, using: :btree
  add_index "situacaos", ["descricao"], name: "index_situacaos_on_descricao", unique: true, using: :btree

  create_table "telefones", force: :cascade do |t|
    t.string   "numero",                limit: 20, null: false
    t.integer  "ramal"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "contato_telefone_id"
    t.string   "contato_telefone_type"
  end

  create_table "tipo_assembleia", force: :cascade do |t|
    t.string   "tipo_assembleia", null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "igreja_id"
  end

  add_index "tipo_assembleia", ["igreja_id"], name: "index_tipo_assembleia_on_igreja_id", using: :btree

  create_table "tipo_documentos", force: :cascade do |t|
    t.string   "tipo",       limit: 50, null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "tipo_enderecos", force: :cascade do |t|
    t.string   "tipo",       limit: 20, null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "tipo_telefones", force: :cascade do |t|
    t.string   "tipo",       limit: 20, null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "cliente_id"
  end

  add_index "tipo_telefones", ["cliente_id"], name: "index_tipo_telefones_on_cliente_id", using: :btree

  create_table "user_igrejas", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.integer  "igreja_id"
    t.integer  "group_id"
  end

  add_index "user_igrejas", ["group_id"], name: "index_user_igrejas_on_group_id", using: :btree
  add_index "user_igrejas", ["igreja_id"], name: "index_user_igrejas_on_igreja_id", using: :btree
  add_index "user_igrejas", ["user_id"], name: "index_user_igrejas_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "login"
    t.string   "email"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "password_digest"
    t.string   "password"
    t.string   "role"
  end

  add_foreign_key "actions", "acos"
  add_foreign_key "arquivos", "assembleia", column: "assembleia_id"
  add_foreign_key "assembleia", "tipo_assembleia", column: "tipo_assembleia_id"
  add_foreign_key "bairros", "cidades"
  add_foreign_key "cidades", "estados"
  add_foreign_key "clientes", "igrejas"
  add_foreign_key "configuracaos", "clientes"
  add_foreign_key "documentos", "pessoas"
  add_foreign_key "documentos", "tipo_documentos"
  add_foreign_key "enderecos", "bairros"
  add_foreign_key "enderecos", "tipo_enderecos"
  add_foreign_key "grau_instrucaos", "clientes"
  add_foreign_key "grau_instrucaos", "pessoas"
  add_foreign_key "groups", "clientes"
  add_foreign_key "igrejas", "clientes"
  add_foreign_key "membro_ocupacaos", "membros"
  add_foreign_key "membro_ocupacaos", "ocupacaos"
  add_foreign_key "membros", "forma_transicaos"
  add_foreign_key "membros", "pessoas"
  add_foreign_key "membros", "profissaos"
  add_foreign_key "membros", "situacaos"
  add_foreign_key "ministerios", "igrejas"
  add_foreign_key "ocupacaos", "ministerios"
  add_foreign_key "permissions", "actions"
  add_foreign_key "pessoa_emails", "emails"
  add_foreign_key "pessoa_emails", "pessoas"
  add_foreign_key "pessoa_telefones", "pessoas"
  add_foreign_key "pessoa_telefones", "telefones"
  add_foreign_key "pessoas", "grau_instrucaos"
  add_foreign_key "pessoas", "igrejas"
  add_foreign_key "pessoas", "situacaos"
  add_foreign_key "profissaos", "clientes"
  add_foreign_key "situacaos", "clientes"
  add_foreign_key "tipo_assembleia", "igrejas"
  add_foreign_key "tipo_telefones", "clientes"
  add_foreign_key "user_igrejas", "groups"
  add_foreign_key "user_igrejas", "igrejas"
  add_foreign_key "user_igrejas", "users"
end
