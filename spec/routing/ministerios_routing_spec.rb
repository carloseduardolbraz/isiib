require "rails_helper"

RSpec.describe MinisteriosController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/ministerios").to route_to("ministerios#index")
    end

    it "routes to #new" do
      expect(:get => "/ministerios/new").to route_to("ministerios#new")
    end

    it "routes to #show" do
      expect(:get => "/ministerios/1").to route_to("ministerios#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/ministerios/1/edit").to route_to("ministerios#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/ministerios").to route_to("ministerios#create")
    end

    it "routes to #update" do
      expect(:put => "/ministerios/1").to route_to("ministerios#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/ministerios/1").to route_to("ministerios#destroy", :id => "1")
    end

  end
end
