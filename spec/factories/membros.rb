FactoryGirl.define do
  	factory :membro do
		data_batismo "2015-05-14"
		data_inativacao "2015-05-14"
		observacao
  	end
  	factory :pessoa do
  		nome "Carlos Eduardo"
		data_nascimento "30/09/1992"
		forma_tratamento "Sr."
		sexo "M"
		nacionalidade "Brasileiro"
		naturalidade "Rio de Janeiro"
  	end
end