FactoryGirl.define do
	

	factory :bairro do
		bairro      "Botafogo"
		cidade
	end

	factory :cidade do
		cidade "Nova Iguaçu"
	end
	
	factory :endereco do
		logradouro  "Rua Do Meier"
		cep         20775160
		numero      119
		complemento 'Casa da praçinha'
		tipo_endereco
	end
	
	factory :tipo_endereco do
		tipo "Casa"
	end
	
	factory :igreja do
	    razao_social  "Igreja Batista Meier"
	    nome_fantasia "IBM"
	    cnpj          "01001000112"
	end
	



	factory :email do
		email "charleycesar@gmail.com"
	end

	factory :telefone do
		numero "67119752"
		observacao "Ramail 264 - Charley"
	end
	
end