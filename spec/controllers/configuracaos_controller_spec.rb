require 'rails_helper'
RSpec.describe ConfiguracaosController, type: :controller do
	describe "CRUD de Configuracao" do
	  	before(:each) do
	    	unless Cliente.exists?    		    		    		
	    		Cliente.create data_cadastro: DateTime.now
	    	end
	    	unless Configuracao.exists?
	    		Configuracao.create cliente_id: Cliente.first().id
	    	end
	  	end
	    
	    it "create - Deve retornar http_status :created e a mensagem  'Configuração cadastrada com sucesso'" do
	    	object_save = { configuracao: { logo: Faker::Company.logo, configurado: true, cliente_id: Cliente.first().id} }
	      post :create, object_save
	      expect(response).to have_http_status(:created)
	      expect(response.body).to eq({data: 'Configuração cadastrada com sucesso!'}.to_json)
	    end

	    it "update - Deve retornar http_status :success e a mensagem um json {data: 'Cliente configurado com sucesso'}" do
	    	object_save = { configuracao: { configurado: true} }
	    	post :update, format: :json,:id => Configuracao.first.id, :configuracao => {:configurado => true}
			expect("t").to eq(Configuracao.first.configurado) 
			expect(response).to have_http_status(:success)
			expect(response.body).to eq({data: "Configurações atualizadas com sucesso!"}.to_json)
	    end
  	end
end
