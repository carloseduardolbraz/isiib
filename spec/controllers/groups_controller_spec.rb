require 'rails_helper'

RSpec.describe GroupsController, type: :controller do

  before(:all) do 
    @aco = Aco.create(name: "membros", description: "Membros")
    @action = Action.new(name: 'index' , description: "Consultar")
    @action.aco = @aco
    @action.save          

  end

  describe "GET #index" do
    it "returns http success" do
      get :index      
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST #create - Na rota seria como POST #index " do
    it "Deve salvar um grupo e suas permissões. Deve retornar no body uma mensagem de Salvo com sucesso e o HTTPStatus deve ser :created" do
      aco = Aco.first
      object_save = {group: {name: "teste"}, permission: [{level: true, action_id: @action.id}], format: :json}
      post :create, object_save 
      puts response.body
      expect(response.body).to eq("O Grupo #{object_save[:group][:name].capitalize} foi cadastrado com sucesso!".to_json)
      #, "Houve um erro ao tentar salvar, verificar os parâmetros se existe (group)" 
      expect(response).to have_http_status(:created)
    end
  end

end
