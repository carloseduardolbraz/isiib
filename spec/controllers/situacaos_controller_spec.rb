require 'rails_helper'
RSpec.describe SituacaosController, type: :controller do

	describe 'POST #create' do
		it "Deve Salvar uma nova situação. Retornando mensagem de sucesso e status 201" do 	
			# envio o symbolo pra chegar como params  na controller
			# Desse jeito posso simular um json atraves do navegador
			post :create, :situacao => FactoryGirl.attributes_for(:situacao) 
			expect(response).to have_http_status(:created)
			#expect(response.body).to eq("Situação cadastrada com sucesso!")
	   
		end
	end

	describe 'PUT #update' do
		it "Atualiza uma situação previamente cadastrada e deve retornar o valor atualizado atraves de um find" do
			situacao = FactoryGirl.create(:situacao)
			post :update, format: :json,:id => situacao.id, :situacao => {:descricao => 'Desativado'}
			expect("Desativado").to eq(Situacao.find(situacao.id).descricao) 
		end
	end
end