require 'rails_helper'
RSpec.describe MinisteriosController, type: :controller do

  describe "GET #index" do
    it "Retorna uma instacia de ministerio igual ao que está na variavel no jbuilder @ministerios" do
      ministerio = FactoryGirl.create(:ministerio)
      get :index, format: :json, {IgrejaId => 1}
      expect(assigns(:ministerios)).to eq([ministerio])
    end
  end

  describe "GET #show" do
    it "Retorna um unico ministério igual ao que foi requerido @ministerio" do
      ministerio = FactoryGirl.create(:ministerio)
      get :show, {:id => ministerio.id}, format: :json
      expect(assigns(:ministerio)).to eq(ministerio)
    end
  end

  describe "POST #create" do
    context "Com parametros válidos" do
      it "Deve criar um novo Ministerio" do
        expect {
          post :create, { :ministerio => FactoryGirl.attributes_for(:ministerio) }
        }.to change(Ministerio, :count).by(1)
      end

      it "Deve criar um novo Ministerio e deve ser igual ao hash de @ministerio" do
        post :create, {:ministerio => FactoryGirl.attributes_for(:ministerio)}
        expect(assigns(:ministerio)).to be_a(Ministerio)
        expect(assigns(:ministerio)).to be_persisted 
      end
    end

    context "Com parametros invalidos" do
      it "Deve tentar criar mais tem que falhar o save de ministerio" do
        post :create, {:ministerio => FactoryGirl.attributes_for(:ministerio, ministerio:"")}
        expect(assigns(:ministerio)).to be_a_new(Ministerio)
      end 
    end
  end

  describe "PUT #update" do
    context "Com parametros válidos" do
      it "Atualiza o ministerio requerido" do
        ministerio = FactoryGirl.create(:ministerio)
        put :update, {:id => ministerio.to_param, :ministerio => {ministerio:'Louvor'} }
        ministerio.reload
        expect("Louvor").to eq(ministerio.ministerio)
      end

      it "Deve atualizar o registro como todos o dados igual ao @ministerio" do
        ministerio = FactoryGirl.create(:ministerio)
        put :update, {:id => ministerio.to_param, :ministerio => FactoryGirl.attributes_for(:ministerio)}
        expect(assigns(:ministerio)).to eq(ministerio)
      end
    end

    context "Com parametros invalidos" do
      it "Deve retornar os dados sem alteração alguma = @ministerio" do
        ministerio = FactoryGirl.create(:ministerio)
        put :update, {:id => ministerio.to_param, :ministerio => FactoryGirl.attributes_for(:ministerio, ministerio: '')}
        expect(assigns(:ministerio)).to eq(ministerio)
      end
    end
  end

  describe "DELETE #destroy" do
    it "Deve deletar o ministerio com o id requerido" do
      ministerio = FactoryGirl.create(:ministerio)
      expect {
        delete :destroy, {:id => ministerio.to_param}
      }.to change(Ministerio, :count).by(-1)
    end
  end

end
