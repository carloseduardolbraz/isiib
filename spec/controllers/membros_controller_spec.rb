require 'rails_helper'
RSpec.describe MembrosController, type: :controller do
describe "POST #create - Na rota seria como POST #index " do
    it "Deve salvar um membro e seus dados de contato retornando mensagem de sucesso e status created" do            
      bairro = FactoryGirl.create(:bairro)
      tipo_endereco = FactoryGirl.create(:tipo_endereco)
      data_save = {
        :pessoa => FactoryGirl.attributes_for(:pessoa), 
        :membro =>FactoryGirl.attributes_for(:membro),
        :observacoes => [FactoryGirl.attributes_for(:observacao)], 
        :enderecos =>[FactoryGirl.attributes_for(:endereco, bairro: bairro.bairro, cidade_id: bairro.cidade_id,tipo_endereco_id: tipo_endereco.id)],
        :telefones =>[FactoryGirl.attributes_for(:telefone, observacao: "Residencial"),FactoryGirl.attributes_for(:telefone, numero: "(21) 2886-1845",observacao: "Celular")],
        :emails =>[FactoryGirl.attributes_for(:email)],
      } 
      post :create, data_save       
      membro_check = Membro.last
      expect(response.body).to eq("Membro Cadastrado com sucesso!")
      expect(response).to have_http_status(:created)
      expect("Carlos Eduardo").to eq(membro_check.pessoa.nome)
      expect("Teste de Observacao").to eq(membro_check.observacaos[0].observacao)
      expect("Rua Do Meier").to eq(membro_check.pessoa.enderecos[0].logradouro)
      expect(2).to eq(membro_check.pessoa.telefones.size)
      expect("Residencial").to eq(membro_check.pessoa.telefones[0].observacaos[0].observacao)
      expect("charleycesar@gmail.com").to eq(membro_check.pessoa.emails[0].email)
      #Para este teste funcionar o banco deve ser alterado o campo de data para timestamp without time zone 
      #expect("14/05/2015").to eq(membro_check.data_batismo.to_s)       
    end
  end
end