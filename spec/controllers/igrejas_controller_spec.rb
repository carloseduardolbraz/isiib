require 'rails_helper'
RSpec.describe IgrejasController, type: :controller do
  describe 'GET /igrejas.json' do    
    it "Retorna sucesso na requisição" do
      igreja = FactoryGirl.create(:igreja)  
      get :index, format: :json
      expect(response).to be_success
    end
  end 
  describe 'GET /igrejas/:id.json' do
    it "Retorna uma igreja" do
      igreja = FactoryGirl.create(:igreja)  
      get :show, format: :json, :id=>igreja.id
      expect(Igreja.find(igreja.id)).to_not be_nil
    end
  end
  describe 'POST /igrejas.json' do
    it "Deve Salvar uma nova igreja. Retornando mensagem de sucesso e status 201" do  
      bairro = FactoryGirl.create(:bairro)
      tipo_endereco = FactoryGirl.create(:tipo_endereco)
      data_save = {
        :igreja => FactoryGirl.attributes_for(:igreja), 
        :email =>FactoryGirl.attributes_for(:email), 
        :endereco =>FactoryGirl.attributes_for(:endereco, bairro: bairro.bairro, cidade_id: bairro.cidade_id,tipo_endereco_id: tipo_endereco.id),
        :telefone =>FactoryGirl.attributes_for(:telefone),
      } 
      post :create, data_save
      expect(response).to have_http_status(:created) 
      expect(response.body).to eq("\"Igreja cadastrada com sucesso!\"") 
    end
  end
  describe 'PUT #update' do
    it "Atualiza uma igreja previamente cadastrada e deve retornar o valor atualizado e status 201" do
      igreja = FactoryGirl.create(:igreja)
      data_save = {
        :igreja => FactoryGirl.attributes_for(:igreja, nome_fantasia: 'Nome fantasia Editado'),
        :endereco =>FactoryGirl.attributes_for(:endereco, bairro: 'Meier', logradouro: 'Teste de logradouro'),
        :email =>FactoryGirl.attributes_for(:email, email: 'programador@lanwise.com.br'),
        :telefone =>FactoryGirl.attributes_for(:telefone, numero: '967119752', observacao: 'Ramail 264 - Charley'),
      } 
      post :update, format: :json,:id => igreja.id, :igreja => data_save[:igreja], :endereco => data_save[:endereco], :email => data_save[:email], :telefone => data_save[:telefone]
      i = Igreja.find(igreja.id)
      expect("Teste de logradouro").to eq(i.enderecos[0].logradouro)
      expect("Meier").to eq(i.enderecos[0].bairro.bairro)
      expect("programador@lanwise.com.br").to eq(i.emails[0].email)
      expect("967119752").to eq(i.telefones[0].numero)
      expect("Ramail 264 - Charley").to eq(i.telefones[0].observacaos[0].observacao)
      expect("Nome fantasia Editado").to eq(i.nome_fantasia)
      expect("\"Dados da Igreja foram atualizados!\"").to eq(response.body) 
    end
  end
  describe 'POST #create_congregacao' do
    it "Deve Cadastrar uma nova congregação. Retornando uma mensage de sucesso e status 201" do
      bairro = FactoryGirl.create(:bairro)
      tipo_endereco = FactoryGirl.create(:tipo_endereco)
      igreja_principal = FactoryGirl.create(:igreja)
      data_save = {
        :igreja => FactoryGirl.attributes_for(:igreja), 
        :email =>FactoryGirl.attributes_for(:email), 
        :endereco =>FactoryGirl.attributes_for(:endereco, bairro: bairro.bairro, cidade_id: bairro.cidade_id,tipo_endereco_id: tipo_endereco.id),
        :telefone =>FactoryGirl.attributes_for(:telefone),
        :id_igreja => igreja_principal.id
      } 
      post :create_congregacao, data_save
      expect(response).to have_http_status(:created) 
      expect(igreja_principal.igrejas.count).to eq(1)
      expect({data: "Congregação cadastrada com sucesso!"}.to_json).to eq(response.body)
    end
  end
  describe 'PUT #update_congregacao' do
    it "Atualiza uma congregação e deve retornar o valor atualizado e status 201" do
      igreja = FactoryGirl.create(:igreja)
      igreja.igrejas << FactoryGirl.create(:igreja)
      congregacao = Igreja.find(igreja.id).igrejas.first
      tipo_endereco = FactoryGirl.create(:tipo_endereco)
      data_save = {
        :congregacao => FactoryGirl.attributes_for(:igreja, nome_fantasia: 'Nome fantasia Editado'),
        :endereco =>FactoryGirl.attributes_for(:endereco, logradouro: 'Teste de logradouro',bairro: 'Meier',tipo_endereco: tipo_endereco.id),
        :email =>FactoryGirl.attributes_for(:email, email: 'programador@lanwise.com.br'),
        :telefone =>FactoryGirl.attributes_for(:telefone, numero: '967119752',observacao: 'Charley - Ramal 2255'),
      } 
      post :update_congregacao, format: :json,:id_igreja => igreja.id,:id_congregacao => congregacao.id, :congregacao => data_save[:congregacao], :endereco => data_save[:endereco], :email => data_save[:email], :telefone => data_save[:telefone]
      congregacao_check = Igreja.find(congregacao.id)
      expect("Teste de logradouro").to eq(congregacao_check.enderecos[0].logradouro)
      expect("Meier").to eq(congregacao_check.enderecos[0].bairro.bairro)
      expect("programador@lanwise.com.br").to eq(congregacao_check.emails[0].email)
      expect("967119752").to eq(congregacao_check.telefones[0].numero)
      expect("Charley - Ramal 2255").to eq(congregacao_check.telefones[0].observacaos[0].observacao)
      expect("Nome fantasia Editado").to eq(congregacao_check.nome_fantasia)
      expect("\"Congregação atualizada com sucesso!\"").to eq(response.body) 
      expect(response).to have_http_status(:created) 
    end
  end
end