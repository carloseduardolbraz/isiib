Rails.application.routes.draw do

  get 'dashboard/show'

  get "/auth/auth0/callback" => "auth0#callback"
  get "/auth/failure" => "auth0#failure"

  get 'ocupacaos/index'
  get "/" => "auth0#login"

  get 'estados' => 'estados#index'
  post 'estados' =>'estados#create'
  post 'groups/cadastrar'

  get 'auth/authenticate'
  get 'sessions/new'
  get 'users/new'

  post 'users/permissions' => 'users#getPermissions'
  post 'login'   => 'users#auntentication'

  resources :tipo_assembleia
  resources :forma_transicaos
  resources :arquivos
  resources :ocupacaos
  resources :assembleia
  resources :forma_ingressos
  resources :pessoas
  resources :igrejas
  resources :configuracaos
  resources :tipo_documentos
  resources :tipo_enderecos
  resources :documentos
  resources :grau_instrucaos
  resources :groups
  resources :profissaos
  resources :situacaos
  resources :users
  resources :acos
  resources :actions
  resources :cidades
  resources :bairros
  resources :membros
  resources :ministerios

  #Rotas personalizadas
  get 'membros/page/:page' => 'membros#index'
  post 'membros/filter' => 'membros#filter'

  post 'congregacao/:id_igreja' => 'igrejas#create_congregacao'
  put 'congregacao/:id_congregacao' => 'igrejas#update_congregacao'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
