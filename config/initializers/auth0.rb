Rails.application.config.middleware.use OmniAuth::Builder do
  provider(
    :auth0,
    '1HBCBdk12g6u9ssQoGViQ8iIRDkX8Glm',
    '_96lbG-6VXMHbdu0iqVZRxyA-HlwYdyT7K9HLbpOV3X9v7x9D4k_7ET43L0Zh6Ip',
    'lanwise.auth0.com',
    callback_path: "/auth/auth0/callback"
  )
end
