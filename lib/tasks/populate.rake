desc 'Popula tabela membro'
task :populate => :environment do	
	40.times do
		membro = Membro.create
		nome = Faker::Name.name
		membro.pessoa = Pessoa.create :data_nascimento => Time.now, :nome => nome, :sexo => 'M', :foto => Faker::Avatar.image
		membro.pessoa.emails << Email.create(:email => Faker::Internet.email)
		membro.pessoa.enderecos << Endereco.create(:logradouro => Faker::Address.street_name, :cep => Faker::Address.zip_code, :numero => Faker::Address.building_number, :tipo_endereco_id=>1, :bairro_id=>4)
		membro.save
	end
end