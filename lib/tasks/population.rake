desc 'Popula tabelas basicas'
task :population =>:environment do	
	# 	Carrega os estados  de acordo com a tabela do IBGE (Assim facilita a consulta via POSTMON)
	# 	Author: Carlos Eduardo Lima Braz	<carloseduardo@lanwise.com.br>
	# 	
	# 	see: http://www.ibge.gov.br/home/geociencias/areaterritorial/principal.shtm
	Estado.all().destroy_all
	estados = JSON.parse '[{"id":"11","estado":"Rondônia"},{"id":"12","estado":"Acre"},{"id":"13","estado":"Amazonas"},{"id":"14","estado":"Roraima"},{"id":"15","estado":"Pará"},{"id":"16","estado":"Amapá"},{"id":"17","estado":"Tocantins"},{"id":"21","estado":"Maranhão"},{"id":"22","estado":"Piauí"},{"id":"23","estado":"Ceará"},{"id":"24","estado":"Rio Grande do Norte"},{"id":"25","estado":"Paraíba"},{"id":"26","estado":"Pernambuco"},{"id":"27","estado":"Alagoas"},{"id":"28","estado":"Sergipe"},{"id":"29","estado":"Bahia"},{"id":"31","estado":"Minas Gerais"},{"id":"32","estado":"Espírito Santo"},{"id":"33","estado":"Rio de Janeiro"},{"id":"35","estado":"São Paulo"},{"id":"41","estado":"Paraná"},{"id":"42","estado":"Santa Catarina"},{"id":"43","estado":"Rio Grande do Sul (*)"},{"id":"50","estado":"Mato Grosso do Sul"},{"id":"51","estado":"Mato Grosso"},{"id":"52","estado":"Goiás"},{"id":"53","estado":"Distrito Federal"}]'
	estados.each do |estado|
		Estado.create estado
	end
end