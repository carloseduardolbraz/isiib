require 'test_helper'
require 'user'
class UserTest < ActiveSupport::TestCase

   test "Verifica Login" do
       #Declarações
       login = "adm"
       time_token = 30.minute.from_now
       usuario = User.new

       result = usuario.logar({login: login, password: "administrador"},time_token)
       
       expec = User.find_by_login(login)   
       expec = expec.authenticate("administrador").to_hash
       expec = {:login=>expec["login"]}

       expec = AuthToken.encode(expec,time_token)

       assert(result, "Logar não retornou nada")      
       assert_equal(expec,result, "Erro ao tentar autencicar o usuário adm")
   end
end
