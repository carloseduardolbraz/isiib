require 'test_helper'

class DocumentosControllerTest < ActionController::TestCase
  setup do
    @documento = documentos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:documentos)
  end

  test "should create documento" do
    assert_difference('Documento.count') do
      post :create, documento: { data_expedicao: @documento.data_expedicao, data_validade: @documento.data_validade, documento: @documento.documento, orgao_expeditor: @documento.orgao_expeditor }
    end

    assert_redirected_to documento_path(assigns(:documento))
  end

  test "should show documento" do
    get :show, id: @documento
    assert_response :success
  end


  test "should update documento" do
    patch :update, id: @documento, documento: { data_expedicao: @documento.data_expedicao, data_validade: @documento.data_validade, documento: @documento.documento, orgao_expeditor: @documento.orgao_expeditor }
    assert_redirected_to documento_path(assigns(:documento))
  end

  test "should destroy documento" do
    assert_difference('Documento.count', -1) do
      delete :destroy, id: @documento
    end

    assert_redirected_to documentos_path
  end
end
