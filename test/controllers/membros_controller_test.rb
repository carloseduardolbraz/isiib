require 'test_helper'

class MembrosControllerTest < ActionController::TestCase
  setup do
    @membro = membros(:one)
  end


  



  test "should update membro" do
    patch :update, id: @membro, membro: { data_batismo: @membro.data_batismo, data_inativacao: @membro.data_inativacao}
    assert_redirected_to membro_path(assigns(:membro))
  end
end
