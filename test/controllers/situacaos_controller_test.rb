require 'test_helper'
class SituacaosControllerTest < ActionController::TestCase
  setup do
    @situacao = situacaos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:situacaos)
  end

  test "should create situacao" do
    assert_difference('Situacao.count') do
      post :create, situacao: { descricao: @situacao.descricao }
    end
  end

  test "should show situacao" do
    get :show, id: @situacao
    assert_response :success
  end

  test "should update situacao" do
    patch :update, id: @situacao, situacao: { descricao: @situacao.descricao }
    assert_redirected_to situacao_path(assigns(:situacao))
  end

  test "should destroy situacao" do
    assert_difference('Situacao.count', -1) do
      delete :destroy, id: @situacao
    end

    assert_redirected_to situacaos_path
  end
end
